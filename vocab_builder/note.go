/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package vocab_builder

import (
  "fmt"
  "strings"
  "net/url"
  "time"

  "github.com/go-fed/activity/pub"
  "github.com/go-fed/activity/streams"
  "github.com/go-fed/activity/streams/vocab"
)

func Note(params map[string]interface{}) (vocab.ActivityStreamsNote, error) {
  toParams, _ := paramsToSlice(params, "to")
  summaryParams, _ := paramsToSlice(params, "summary")
  contentParams, ok := paramsToSlice(params, "content")
  if !ok {
    return nil, fmt.Errorf("content is a mandatory field")
  }
  _, public := paramsToSlice(params, "public")

  note := streams.NewActivityStreamsNote()
  publishedProp := streams.NewActivityStreamsPublishedProperty()
  publishedProp.Set(time.Now())
  note.SetActivityStreamsPublished(publishedProp)

  toProp := streams.NewActivityStreamsToProperty()
  // skip if we haven't explicitly named an endpoint
  if len(toParams) > 0 {
    // it can be possible that every entry contains multiple endpoints
    tos := strings.Split(strings.Join(toParams, ","), ",")
    for _, t := range tos {
      toIRI, err := url.Parse(t)
      if err != nil {
        return nil, err
      }
      toProp.AppendIRI(toIRI)
    }
  }
  if public {
    publicIRI, err := url.Parse(pub.PublicActivityPubIRI)
    if err != nil {
      return nil, err
    }
    toProp.AppendIRI(publicIRI)
  }
  note.SetActivityStreamsTo(toProp)
  if len(summaryParams) > 0 {
    summaryProp := streams.NewActivityStreamsSummaryProperty()
    summaryProp.AppendXMLSchemaString(strings.Join(summaryParams, "\n"))
    note.SetActivityStreamsSummary(summaryProp)
  }
  contentProp := streams.NewActivityStreamsContentProperty()
  contentProp.AppendXMLSchemaString(strings.Join(contentParams, "\n"))
  note.SetActivityStreamsContent(contentProp)
  return note, nil
}
