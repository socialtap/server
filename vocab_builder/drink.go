/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package vocab_builder

import (
  "fmt"
  "strconv"
  "net/url"

  "github.com/go-fed/activity/streams"
  "github.com/go-fed/activity/streams/vocab"
)

func Drink(params map[string]interface{}) (vocab.SocialtapDrink, error) {
  nameParams, ok := paramsToSlice(params, "drinkName")
  if !ok {
    return nil, fmt.Errorf("name is a mandatory field")
  }
  mediaParams, _ := paramsToSlice(params, "drinkMedia")
  styleParams, _ := paramsToSlice(params, "drinkStyle")
  categoryParams, ok := paramsToSlice(params, "drinkCategory")
  if !ok {
    return nil, fmt.Errorf("category is a mandatory field")
  }
  abvParams, _ := paramsToSlice(params, "drinkAbv")
  _, active := paramsToSlice(params, "drinkActive")

  drink := streams.NewSocialtapDrink()
  nameProp := streams.NewSocialtapNameProperty()
  imageNameProp := streams.NewActivityStreamsNameProperty()
  for _, name := range nameParams {
    nameProp.AppendXMLSchemaString(name)
    imageNameProp.AppendXMLSchemaString(name)
  }
  drink.SetSocialtapName(nameProp)

  activeProp := streams.NewSocialtapActiveProperty()
  activeProp.AppendXMLSchemaBoolean(active)
  drink.SetSocialtapActive(activeProp)

  if len(abvParams) > 0 {
    abv, err := strconv.ParseFloat(abvParams[0], 64)
    if err != nil {
      return nil, err
    }
    abvProp := streams.NewSocialtapAbvProperty()
    abvProp.AppendXMLSchemaFloat(abv)
    drink.SetSocialtapAbv(abvProp)
  }

  styleProp := streams.NewSocialtapStyleProperty()
  for _, style := range styleParams {
    styleProp.AppendXMLSchemaString(style)
  }
  drink.SetSocialtapStyle(styleProp)

  if len(mediaParams) > 0 {
    mediaIRI, err := url.Parse(mediaParams[0])
    if err != nil {
      return nil, err
    }
    imageUrl := streams.NewActivityStreamsUrlProperty()
    imageUrl.AppendXMLSchemaAnyURI(mediaIRI)
    image := streams.NewActivityStreamsImage()
    image.SetActivityStreamsUrl(imageUrl)
    image.SetActivityStreamsName(imageNameProp)
    imageProp := streams.NewSocialtapImageProperty()
    imageProp.AppendActivityStreamsImage(image)
    drink.SetSocialtapImage(imageProp)
  }

  categoryProp := streams.NewSocialtapCategoryProperty()
  for _, category := range categoryParams {
    categoryProp.AppendXMLSchemaString(category)
  }
  drink.SetSocialtapCategory(categoryProp)
  return drink, nil
}
