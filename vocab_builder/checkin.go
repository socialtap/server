/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package vocab_builder

import (
  "fmt"
  "strconv"
  "net/url"
  "time"

  "github.com/go-fed/activity/pub"
  "github.com/go-fed/activity/streams"
  "github.com/go-fed/activity/streams/vocab"
  "github.com/go-fed/apcore/util"
)

func Checkin(params map[string]interface{}) (vocab.SocialtapCheckin, error) {
  mediaParams, _ := paramsToSlice(params, "checkinMedia")
  ratingParams, _ := paramsToSlice(params, "checkinRating")
  commentParams, _ := paramsToSlice(params, "checkinComment")
  _, public := paramsToSlice(params, "public")

  checkin := streams.NewSocialtapCheckin()

  // only if this checkin was imported from untappd
  if untappdCheckinIds, ok := paramsToSlice(params, "untappdCheckinId"); ok {
    untappdIdProp := streams.NewSocialtapUntappdCheckinIdProperty()
    for _, id := range untappdCheckinIds {
      untappdId, err := strconv.ParseFloat(id, 64)
      if err != nil {
        return nil, err
      }
      untappdIdProp.AppendXMLSchemaFloat(untappdId)
    }
    checkin.SetSocialtapUntappdCheckinId(untappdIdProp)
  }

  publishedProp := streams.NewActivityStreamsPublishedProperty()
  if publishedParams, ok := paramsToSlice(params, "published"); ok && len(publishedParams) > 0 {
    published, err := time.Parse(time.RFC3339, publishedParams[0])
    if err != nil {
      return nil, err
    }
    publishedProp.Set(published)
  } else {
    publishedProp.Set(time.Now())
  }
  checkin.SetActivityStreamsPublished(publishedProp)

  if public {
    toProp := streams.NewActivityStreamsToProperty()
    publicIRI, err := url.Parse(pub.PublicActivityPubIRI)
    if err != nil {
      return nil, err
    }
    toProp.AppendIRI(publicIRI)
    checkin.SetActivityStreamsTo(toProp)
  }

  drinkProp := streams.NewSocialtapDrinkProperty()
  drink, err := Drink(params)
  if err != nil {
    return nil, err
  }
  drinkProp.AppendSocialtapDrink(drink)
  checkin.SetSocialtapDrink(drinkProp)

  manufacturerProp := streams.NewSocialtapManufacturerProperty()
  manufacturer, err := Manufacturer(params)
  if err != nil {
    return nil, err
  }
  manufacturerProp.AppendSocialtapManufacturer(manufacturer)
  checkin.SetSocialtapManufacturer(manufacturerProp)

  venue, err := Venue(params)
  if err == nil {
    venueProp := streams.NewSocialtapVenueProperty()
    venueProp.AppendSocialtapVenue(venue)
    checkin.SetSocialtapVenue(venueProp)
  } else {
    util.InfoLogger.Infof("skipping venue for checkin: %s", err)
  }

  if len(ratingParams) > 0 {
    var (
      err error
      max int64 = 5
      user float64 = 0
    )

    user, err = strconv.ParseFloat(ratingParams[0], 64)
    if err != nil {
      return nil, err
    }

    if len(ratingParams) > 1 {
      max, err = strconv.ParseInt(ratingParams[1], 10, 64)
      if err != nil {
        return nil, err
      }
    }

    if float64(max) < user {
      return nil, fmt.Errorf("user rating %d must be greater or equal to %d", user, max)
    }

    maxProp := streams.NewSocialtapMaxProperty()
    maxProp.AppendXMLSchemaNonNegativeInteger(int(max))
    userProp := streams.NewSocialtapUserProperty()
    userProp.AppendXMLSchemaFloat(user)

    rating := streams.NewSocialtapRating()
    rating.SetSocialtapMax(maxProp)
    rating.SetSocialtapUser(userProp)

    ratingProp := streams.NewSocialtapRatingProperty()
    ratingProp.AppendSocialtapRating(rating)
    checkin.SetSocialtapRating(ratingProp)
  }

  if len(commentParams) > 0 {
    contentProp := streams.NewActivityStreamsContentProperty()
    for _, comment := range commentParams {
      contentProp.AppendXMLSchemaString(comment)
    }
    checkin.SetActivityStreamsContent(contentProp)
  }

  if len(mediaParams) > 0 {
    imageProp := streams.NewActivityStreamsImageProperty()
    for _, mediaParam := range mediaParams {
      mediaIRI, err := url.Parse(mediaParam)
      if err != nil {
        return nil, err
      }
      imageUrl := streams.NewActivityStreamsUrlProperty()
      imageUrl.AppendXMLSchemaAnyURI(mediaIRI)
      image := streams.NewActivityStreamsImage()
      image.SetActivityStreamsUrl(imageUrl)
      // image.SetActivityStreamsName(imageNameProp)
      imageProp.AppendActivityStreamsImage(image)
    }
    checkin.SetActivityStreamsImage(imageProp)
  }
  return checkin, nil
}
