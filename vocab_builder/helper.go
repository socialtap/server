/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package vocab_builder

import (
  "fmt"
  "strings"
  "time"

  "github.com/mdlayher/untappd"
  "github.com/go-fed/activity/streams/vocab"
  "github.com/go-fed/apcore/util"
)

// ConvertUntappdCheckin will turn a untappd.Checkin into an vocab.SocialtapCheckin
// by parsing the untappd.Checkin fields into a map and running the normal vocab builder
func ConvertUntappdCheckin(checkin *untappd.Checkin) (vocab.SocialtapCheckin, error) {
  if checkin.Beer == nil {
    return nil, fmt.Errorf("checkin.Beer is mandatory")
  }

  if checkin.Brewery == nil {
    return nil, fmt.Errorf("checkin.Brewery is mandatory")
  }

  params := make(map[string]interface{})
  // when was the checkin created/published
  params["published"] = []string{checkin.Created.Format(time.RFC3339)}

  // checkin
  //
  // checkin_media=https://untappd.akamaized.net/photos/2021_03_23/15ce52d1e5af86c3eb5c5ff7f66cf931_1280x1280.jpg
  // checkin_rating=4.5
  // checkin_comment=Awesome stuff!
  //
  var mediaSlice []string
  for _, media := range checkin.Media {
    mediaSlice = append(mediaSlice, media.Photo.Og.String())
  }
  if len(mediaSlice) > 0 {
    params["checkinMedia"] = mediaSlice
  }
  params["checkinRating"] = []string{fmt.Sprintf("%.4f", checkin.UserRating)}
  params["checkinComment"] = []string{checkin.Comment}
  // beer/drink
  //
  // drink_name=Tepache Sour
  // drink_media=https://untappd.akamaized.net/site/beer_logos/beer-4138042_6540b_sm.jpeg
  // drink_style=Sour - Fruited
  // drink_category=Beer
  // drink_abv=5
  // drink_active=1
  //
  params["drinkCategory"] = []string{"Beer"}
  params["drinkName"] = []string{checkin.Beer.Name}
  params["drinkMedia"] = []string{checkin.Beer.Label.String()}
  params["drinkStyle"] = []string{checkin.Beer.Style}
  params["drinkAbv"] = []string{fmt.Sprintf("%.4f", checkin.Beer.ABV)}
  if checkin.Brewery.Active {
    params["drinkActive"] = []string{"1"}
  }
  // brewery
  //
  // manufacturer_name=Alvarium Beer Company
  // manufacturer_active=1
  // manufacturer_url=http://www.alvariumbeer.com
  // manufacturer_media=https://untappd.akamaized.net/site/brewery_logos/brewery-320859_2721c.jpeg
  // manufacturer_category=Brewery
  // manufacturer_location=41.6641
  // manufacturer_location=-72.7548
  // manufacturer_location=New Britain, CT
  //
  params["manufacturerName"] = []string{checkin.Brewery.Name}
  if checkin.Brewery.Active {
    params["manufacturerActive"] = []string{"1"}
  }
  params["manufacturerUrl"] = []string{checkin.Brewery.Contact.URL}
  params["manufacturerMedia"] = []string{checkin.Brewery.Logo.String()}
  params["manufacturerCategory"] = []string{"Brewery"}
  params["manufacturerLocation"] = []string{
    fmt.Sprintf("%.4f", checkin.Brewery.Location.Latitude),
    fmt.Sprintf("%.4f", checkin.Brewery.Location.Longitude),
    strings.Join([]string{
      checkin.Brewery.Location.City, checkin.Brewery.Location.State,
    }, ", "),
  }
  // venue
  //
  // venue_name=Untappd at Home
  // venue_media=https://untappd.akamaized.net/venuelogos/venue_9917985_b3a5d245_bg_176.png
  // venue_url=https://untappd.com/v/untappd-at-home/9917985
  // venue_category=Residence
  // venue_location=34.2347
  // venue_location=-77.9482
  // venue_location=Everywhere, United States
  //
  if checkin.Venue != nil {
    params["venueName"] = []string{checkin.Venue.Name}
    params["venueMedia"] = []string{checkin.Venue.Icon.LargeIcon.String()}
    params["venueUrl"] = []string{checkin.Venue.Foursquare.URL}
    params["venueCategory"] = []string{checkin.Venue.Category}
    params["venueLocation"] = []string{
      fmt.Sprintf("%.4f", checkin.Venue.Location.Latitude),
      fmt.Sprintf("%.4f", checkin.Venue.Location.Longitude),
      strings.Join([]string{
        checkin.Venue.Location.Address,
        checkin.Venue.Location.City,
        checkin.Venue.Location.State,
        checkin.Venue.Location.Country,
      }, ", "),
    }
  }
  // everything in untappd is public
  params["public"] = []string{"1"}
  // all untappd checkins should be remembered as such
  params["untappdCheckinId"] = []string{fmt.Sprintf("%d", checkin.ID)}
  return Checkin(params)
}

func paramsToSlice(params map[string]interface{}, key string) (slice []string, ok bool) {
  param, ok := params[key]; if !ok {
    return
  }

  switch entry := param.(type) {
  case []string:
    slice = entry
    ok = true
  case []interface{}:
    for _, item := range entry {
      if s, okk := entryToString(item); okk {
        slice = append(slice, s)
      } else {
        ok = false
        return
      }
    }
    ok = true
  case interface{}:
    if s, okk := entryToString(entry); okk {
      ok = true
      slice = append(slice, s)
    }
  case nil:
    ok = true
  default:
    ok = false
    util.ErrorLogger.Infof("cannot type cast %T", param)
  }
  return
}

func entryToString(entry interface{}) (s string, ok bool) {
  ok = true
  switch item := entry.(type) {
  case float64:
    s = fmt.Sprintf("%g", item)
  case string:
    s = item
  case int64:
    s = fmt.Sprintf("%d", item)
  case bool:
    s = fmt.Sprintf("%t", item)
  default:
    ok = false
    util.ErrorLogger.Infof("cannot type cast %T", entry)
  }
  return
}
