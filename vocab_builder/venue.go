/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package vocab_builder

import (
  "fmt"
  "net/url"

  "github.com/go-fed/activity/streams"
  "github.com/go-fed/activity/streams/vocab"
  "github.com/go-fed/apcore/util"
)

func Venue(params map[string]interface{}) (vocab.SocialtapVenue, error) {
  nameParams, ok := paramsToSlice(params, "venueName")
  if !ok {
    return nil, fmt.Errorf("name is a mandatory field")
  }
  mediaParams, _ := paramsToSlice(params, "venueMedia")
  urlParams, _ := paramsToSlice(params, "venueUrl")
  categoryParams, _ := paramsToSlice(params, "venueCategory")
  if !ok {
    return nil, fmt.Errorf("category is a mandatory field")
  }

  venue := streams.NewSocialtapVenue()
  nameProp := streams.NewSocialtapNameProperty()
  imageNameProp := streams.NewActivityStreamsNameProperty()
  for _, name := range nameParams {
    nameProp.AppendXMLSchemaString(name)
    imageNameProp.AppendXMLSchemaString(name)
  }
  venue.SetSocialtapName(nameProp)

  if len(mediaParams) > 0 {
    mediaIRI, err := url.Parse(mediaParams[0])
    if err != nil {
      return nil, err
    }
    imageUrl := streams.NewActivityStreamsUrlProperty()
    imageUrl.AppendXMLSchemaAnyURI(mediaIRI)
    image := streams.NewActivityStreamsImage()
    image.SetActivityStreamsUrl(imageUrl)
    image.SetActivityStreamsName(imageNameProp)
    imageProp := streams.NewSocialtapImageProperty()
    imageProp.AppendActivityStreamsImage(image)
    venue.SetSocialtapImage(imageProp)
  }

  categoryProp := streams.NewSocialtapCategoryProperty()
  for _, category := range categoryParams {
    categoryProp.AppendXMLSchemaString(category)
  }
  venue.SetSocialtapCategory(categoryProp)

  if len(urlParams) > 0 {
    urlProp := streams.NewSocialtapUrlProperty()
    for _, param := range urlParams {
      iri, err := url.Parse(param)
      if err != nil {
        return nil, err
      }
      urlProp.AppendIRI(iri)
    }
    venue.SetSocialtapUrl(urlProp)
  }

  location, err := LocationProperty(params, streams.SocialtapVenueName)
  if err == nil {
    venue.SetSocialtapLocation(location)
  } else {
    util.InfoLogger.Infof("skipping location for drink: %s", err)
  }
  return venue, nil
}
