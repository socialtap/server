/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2019 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
  "os"

  "github.com/go-fed/apcore"
  "github.com/go-fed/apcore/app"
  "github.com/go-fed/apcore/framework"
  "github.com/go-fed/apcore/util"
  "github.com/go-fed/apcore/services"
  "github.com/robfig/cron"
  "git.feneas.org/socialtap/server/workers"
  "git.feneas.org/socialtap/server/activity"
  "git.feneas.org/socialtap/server/database"
)

func main() {
  var (
    err error = nil
    debug bool = false
    application = &activity.Application{
      Scheme: "https",
    }
  )

  application.Config, err = framework.LoadConfigFile("config.ini", application, debug)
  if err != nil {
    util.ErrorLogger.Warningln("config is not setup. Please run 'new' command first!")
    apcore.Run(application)
    return
  }

  _, err = os.Stat(application.Config.ServerConfig.CookieAuthKeyFile)
  if os.IsNotExist(err) {
    err = services.CreateKeyFile(application.Config.ServerConfig.CookieAuthKeyFile)
    if err != nil {
      util.ErrorLogger.Fatal(err)
    }
  }

  // run apcore database migrations
  // XXX get rid of apcore or fully integrate it
  database.InitApcoreDatabaseSetup(
    application.Scheme, application.Config, application)

  // cron initialization should happen after the application server was started
  application.PostBuildRoutes = func(r app.Router, db app.Database, f app.Framework) error {
    // register socialtap worker
    if application.Config.SocialtapConfig.Worker {
      util.InfoLogger.Infoln("start cronjobs..")
      cronjob := cron.New()
      cronjob.AddFunc("@every 1h", workers.UntappdHandler(
        application.Config, application.Scheme, f, application, db))
      cronjob.Start()
    } else {
      util.InfoLogger.Warningln("Workers are disabled!")
    }
    return nil
  }

  // start activitypub application server
  apcore.Run(application)
}
