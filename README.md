# Socialtap - drink socially

## Build

### Compile assets

    yarn install
    yarn build

### Compile binary

    go build -o socialtap

#### GLIBC

If you have to compile for a certain libc version you can do it by running:

    docker run -v $HOME/.golang:/go -v $(pwd):/build --rm -ti golang:buster bash
    > cd /build && go build -o socialtap

## Docker image

    docker build -t socialtap/server:dev .

## Installation

### Database

    create database socialtap;
    \c socialtap

Create the database in Postgres and configure authentication according the settings in your `config.ini`.
Then install the following extensions:

Text similarity measurement and index searching based on trigrams

    create extension pg_trgm;

Cryptographic functions

    create extension pgcrypto;

Done! The application server will create the tables and settings automatically.
