/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package crypto

import (
  "crypto/rand"
  "fmt"

  "golang.org/x/crypto/bcrypt"
)

type BcryptParams struct {
  SaltSize int
  BCryptStrength int
}

func Bcrypt(params BcryptParams, password string) (salt, hash []byte, err error) {
  if params.SaltSize < 16 {
    params.SaltSize = 16
  }
  salt = make([]byte, params.SaltSize)
  var n int
  n, err = rand.Read(salt)
  if err != nil {
    return
  } else if n != params.SaltSize {
    err = fmt.Errorf("crypto/rand only read %d of %d bytes", n, params.SaltSize)
    return
  }

  salty := append([]byte(password), salt...)
  hash, err = bcrypt.GenerateFromPassword(salty, params.BCryptStrength)
  return
}

func BcryptEquals(password string, salt, hash []byte) bool {
  salty := append([]byte(password), salt...)
  err := bcrypt.CompareHashAndPassword(hash, salty)
  return err == nil
}
