FROM golang:buster

RUN apt-get update && apt-get install -y npm
RUN npm install --global yarn

ADD . /go/src/build
RUN rm -r /go/src/build/dist /go/src/build/node_modules

WORKDIR /go/src/build
RUN yarn install
RUN yarn build
RUN go build -o socialtap

FROM debian:buster

RUN apt-get update && apt-get install -y ca-certificates
RUN apt-get clean && apt-get autoclean

RUN adduser --disabled-password app

COPY --from=0 /go/src/build/dist/ /home/app/dist/
COPY --from=0 /go/src/build/socialtap /home/app/
COPY --from=0 /go/src/build/config.ini.example /home/app/config.ini

RUN chmod +x /home/app/socialtap

WORKDIR /home/app

EXPOSE 80

ENTRYPOINT ["/home/app/socialtap"]
