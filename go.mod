module git.feneas.org/socialtap/server

go 1.16

replace github.com/mdlayher/untappd => git.feneas.org/socialtap/untappd-library v0.1.0

replace github.com/go-fed/activity => git.feneas.org/socialtap/activity v0.1.1-0.20210419142005-e608f0bae9a9

replace github.com/go-fed/apcore => git.feneas.org/socialtap/apcore v0.1.8

require (
	github.com/go-fed/activity v1.0.1-0.20201213224552-472d90163f3a
	github.com/go-fed/apcore v0.0.0-20201226104916-c90c5219d7ba
	github.com/go-gomail/gomail v0.0.0-20160411212932-81ebce5c23df
	github.com/google/uuid v1.2.0
	github.com/gorilla/mux v1.8.0
	github.com/mdlayher/untappd v0.0.0-00010101000000-000000000000
	github.com/microcosm-cc/bluemonday v1.0.7
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pelletier/go-toml v1.4.0 // indirect
	github.com/robfig/cron v1.2.0
	github.com/sams96/rgeo v1.1.1
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	golang.org/x/time v0.0.0-20190308202827-9d24e82272b4 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df // indirect
)
