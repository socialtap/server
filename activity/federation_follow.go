/*
 * Socialtap - Drink socially
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package activity

import (
  "net/url"
  "context"

  "github.com/go-fed/apcore/util"
  "github.com/go-fed/apcore/paths"
  "github.com/go-fed/activity/streams/vocab"
  "git.feneas.org/socialtap/server/database"
  "github.com/go-fed/apcore/app"
  "github.com/go-fed/apcore/framework/config"
)

func OnFederationFollow(scheme string, config *config.Config, a app.Application) func(context.Context, vocab.ActivityStreamsFollow) error {

  return func(c context.Context, v vocab.ActivityStreamsFollow) error {
    remote := v.GetActivityStreamsActor()
    local := v.GetActivityStreamsObject()
    localLoop:
    for localIter := local.Begin(); localIter != local.End(); localIter = localIter.Next() {
      if !localIter.IsIRI() {
        util.ErrorLogger.Infof("FederatingWrappedCallbacks supports IRIs only")
        continue localLoop
      }
      localIRI := localIter.GetIRI()
      remoteLoop:
      for iter := remote.Begin(); iter != remote.End(); iter = iter.Next() {
        if !iter.IsIRI() {
          util.ErrorLogger.Infof("FederatingWrappedCallbacks supports IRIs only")
          continue remoteLoop
        }
        remoteIRI := iter.GetIRI()
        exists, err := database.FollowersContainsForActor(scheme, config, a, localIRI, remoteIRI)
        if err == nil && exists {
          util.ErrorLogger.Infof("%s is already following %s", localIRI.String(), remoteIRI.String())
          continue remoteLoop
        }
        localFollowerIRI, err := url.Parse(localIRI.String() + "/" + paths.FollowersPathKey)
        if err != nil {
          util.ErrorLogger.Infof("cannot parse remote follower IRI: %s", err)
          continue remoteLoop
        }
        err = database.FollowersPrependItem(scheme, config, a, localFollowerIRI, remoteIRI)
        if err != nil {
          util.ErrorLogger.Infof("cannot prepend item to followers: %s", err)
          continue remoteLoop
        }
        util.ErrorLogger.Infof("%s started following %s", localIRI.String(), remoteIRI.String())
      }
    }
    return nil
  }
}
