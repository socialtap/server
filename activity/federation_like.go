/*
 * Socialtap - Drink socially
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package activity

import (
  "context"

  "github.com/go-fed/activity/streams/vocab"
)

func OnFederationLike(c context.Context, v vocab.ActivityStreamsLike) error {
  //m, err := streams.Serialize(v)
  //util.ErrorLogger.Infoln("fwc.Like", m, err)
  // INFO : 2021/04/16 19:43:15.706717 activity.go:125: fwc.Like map[@context:https://www.w3.org/ns/activitystreams actor:https://diaspodon.fr/users/zauberstuhl id:https://diaspodon.fr/users/zauberstuhl#likes/238663 object:https://dev.unmappd.site/api/ap/notes/d4278a1f-1faf-423a-b40b-a26839db3728 type:Like] <nil>
  return nil
}
