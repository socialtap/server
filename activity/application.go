/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package activity

import (
  "fmt"
  "net/http"
  "context"

  "github.com/google/uuid"
  "github.com/go-fed/apcore/framework/config"
  "github.com/go-fed/apcore/framework/web"
  "github.com/go-fed/apcore/framework/oauth2"
  "github.com/go-fed/apcore/app"
  "github.com/go-fed/apcore/util"
  "github.com/go-fed/activity/pub"
  "github.com/go-fed/activity/streams"
  "github.com/go-fed/activity/streams/vocab"
  "github.com/mdlayher/untappd"
  "git.feneas.org/socialtap/server/handlers"
)

const apiVersion = "v0"

var _ app.Application = &Application{}
var _ app.S2SApplication = &Application{}
var _ app.C2SApplication = &Application{}

type Application struct {
  Config *config.Config
  Scheme string
  Sessions *web.Sessions
  OAuth *oauth2.Server
  // PostBuildRoutes executes after BuildRoutes was invoked
  PostBuildRoutes func(app.Router, app.Database, app.Framework) error
}

// GetInboxWebHandlerFunc returns a function rendering the outbox. The framework
// passes in a public-only or private view of the outbox, depending on the
// authorization of the incoming request.
func (a *Application) GetInboxWebHandlerFunc(f app.Framework) func(http.ResponseWriter, *http.Request, vocab.ActivityStreamsOrderedCollectionPage) {
  return handlers.AppInbox(a.Config, f)
}

// ScopePermitsPostOutbox ensures the OAuth2 token scope is "loggedin", which
// is the only permission. Other applications can have more granular
// authorization systems.
func (*Application) ScopePermitsPostOutbox(scope string) (permitted bool, err error) {
  return scope == "postOutbox" || scope == "all", nil
}

// ApplyFederatingCallbacks lets us provide hooks for our application based on
// incoming ActivityStreams data from peer servers.
func (a *Application) ApplyFederatingCallbacks(fwc *pub.FederatingWrappedCallbacks) (others []interface{}) {
  fwc.Follow = OnFederationFollow(a.Scheme, a.Config, a)
  fwc.Like = OnFederationLike

  // Here we add new behavior to our application.
  //
  // The new behavior is to print out Listen activities to Stdout.
  others = []interface{}{
    func(c context.Context, listen vocab.ActivityStreamsListen) error {
      m, err := streams.Serialize(listen)
      util.InfoLogger.Infoln("listen", m, err)
      return nil
    },
  }
  return
}

// ApplySocialCallbacks lets us provide hooks for our application based on
// incoming ActivityStreams data from a user's ActivityPub client.
func (*Application) ApplySocialCallbacks(swc *pub.SocialWrappedCallbacks) (others []interface{}) {
  // Here we add no new C2S Behavior. Doing nothing in this function will
  // let the framework handle the suggested C2S side effects.
  return
}

// This example server software supports the Social API, C2S. We don't have to.
// But it makes sense to support one of {C2S, S2S}, otherwise what are you
// doing here?
func (*Application) C2SEnabled() bool {
  return true
}

// This example server software supports the Federation API, S2S. We don't have
// to. But it makes sense to support one of {C2S, S2S}, otherwise what are you
// doing here?
func (*Application) S2SEnabled() bool {
  return true
}

// Start is called at the beginning of a server's lifecycle, after
// configuration processing and after the database connection is opened
// but before web traffic is being served.
//
// If an error is returned, then the startup process fails.
func (Application) Start() error {
  return nil
}

// Stop is called at the end of a server's lifecycle, after the web
// servers have stopped serving traffic but before the database is
// closed.
//
// If an error is returned, shutdown continues but an error is reported.
func (Application) Stop() error {
  return nil
}

// Returns a pointer to the configuration struct used by the specific
// application. It will be used to save and load from configuration
// files. This object will be passed to SetConfiguration after it is
// loaded from file.
//
// It is expected the Application will return an object with sane
// defaults. The object's struct definition may have struct tags
// supported by gopkg.in/ini.v1 for additional customization. For
// example, the "comment" struct tag is much appreciated by admins.
// Also, it is very important that keys to not collide, so prefix your
// configuration options with a common prefix:
//
//     type MyAppConfig struct {
//         SomeKey string `ini:"my_app_some_key" comment:"Description of this key"`
//     }
//
// This configuration object is intended to be stable for the lifetime
// of a running application. When the command to "serve" is given, this
// function is only called once during application initialization.
//
// The command to "configure" will append these defaults to the guided
// flow. Admins will then be able to inspect the file and modify the
// configuration if desired.
//
// However, sane defaults for an application are incredibly important,
// as the "new" command guides an admin through the creation process
// all the way to serving without interruption. So have sane defaults!
func (Application) NewConfiguration() interface{} {
  return nil
}

// Sets the configuration. The parameter's type is the same type that
// is returned by NewConfiguration. Return an error if the configuration
// is invalid.
//
// This configuration object is intended to be stable for the lifetime
// of a running application. When the command to serve, is given, this
// function is only called once during application initialization.
func (Application) SetConfiguration(config interface{}) error {
  return nil
}

// The handler for the application's "404 Not Found" webpage.
func (a *Application) NotFoundHandler(f app.Framework) http.Handler {
  return handlers.AppNotFound(a.Config, f)
}

// The handler when a request makes an unsupported HTTP method against
// a URI.
func (a *Application) MethodNotAllowedHandler(f app.Framework) http.Handler {
  return handlers.AppMethodNotAllowed(a.Config, f)
}

// The handler for an internal server error.
func (a *Application) InternalServerErrorHandler(f app.Framework) http.Handler {
  return handlers.AppInternalServerError(a.Config, f)
}

// The handler for a bad request.
func (a *Application) BadRequestHandler(f app.Framework) http.Handler {
  return handlers.AppBadRequest(a.Config, f)
}

// Web handler for a GET call to the login page.
//
// It should render a login page that POSTs to the "/login" endpoint.
//
// If the URL contains a query parameter "login_error" with a value of
// "true", then it should convey to the user that the email or password
// previously entered was incorrect.
func (a *Application) GetLoginWebHandlerFunc(f app.Framework) http.HandlerFunc {
  return handlers.AppLogin(a.Config, f)
}

// Web handler for a GET call to the OAuth2 authorization page.
//
// It should render UX that informs the user that the other application
// is requesting to be authorized as that user to obtain certain scopes.
//
// See the OAuth2 RFC 6749 for more information.
func (a *Application) GetAuthWebHandlerFunc(f app.Framework) http.HandlerFunc {
  return handlers.AppAuth(a.Config, f)
}

// Web handler for a call to GET an actor's outbox. The framework
// applies OAuth2 authorizations to fetch a public-only or private
// snapshot of the outbox, and passes it to this handler function.
//
// The builtin ActivityPub handler will use the OAuth authorization.
//
// Returning a nil handler is allowed, and doing so results in only
// ActivityStreams content being served.
func (a *Application) GetOutboxWebHandlerFunc(f app.Framework) func(http.ResponseWriter, *http.Request, vocab.ActivityStreamsOrderedCollectionPage) {
  return handlers.AppOutbox(a.Config, f)
}

// Web handler for a call to GET an actor's followers collection. The
// framework has no authorization requirements to view a user's
// followers collection.
//
// Also returns for the corresponding app.AuthorizeFunc handler, which will
// be applied to both ActivityPub and web requests.
//
// Returning a nil handler is allowed, and doing so results in only
// ActivityStreams content being served. Returning a nil app.AuthorizeFunc
// results in public access.
func (a Application) GetFollowersWebHandlerFunc(f app.Framework) (app.CollectionPageHandlerFunc, app.AuthorizeFunc) {
  return handlers.AppFollowers(a.Config, f)
}

// Web handler for a call to GET an actor's following collection. The
// framework has no authorization requirements to view a user's
// following collection.
//
// Also returns for the corresponding app.AuthorizeFunc handler, which will
// be applied to both ActivityPub and web requests.
//
// Returning a nil handler is allowed, and doing so results in only
// ActivityStreams content being served. Returning a nil app.AuthorizeFunc
// results in public access.
func (a Application) GetFollowingWebHandlerFunc(f app.Framework) (app.CollectionPageHandlerFunc, app.AuthorizeFunc) {
  return handlers.AppFollowing(a.Config, f)
}

// Web handler for a call to GET an actor's liked collection. The
// framework has no authorization requirements to view a user's
// liked collection.
//
// Also returns for the corresponding app.AuthorizeFunc handler, which will
// be applied to both ActivityPub and web requests.
//
// Returning a nil handler is allowed, and doing so results in only
// ActivityStreams content being served. Returning a nil app.AuthorizeFunc
// results in public access.
func (a Application) GetLikedWebHandlerFunc(f app.Framework) (app.CollectionPageHandlerFunc, app.AuthorizeFunc) {
  return handlers.AppLiked(a.Config, f)
}

// Web handler for a call to GET an actor. The framework has no
// authorization requirements to view a user, like a profile.
//
// Also returns for the corresponding app.AuthorizeFunc handler, which will
// be applied to both ActivityPub and web requests.
//
// Returning a nil handler is allowed, and doing so results in only
// ActivityStreams content being served. Returning a nil app.AuthorizeFunc
// results in public access.
func (a Application) GetUserWebHandlerFunc(f app.Framework) (app.VocabHandlerFunc, app.AuthorizeFunc) {
  return handlers.AppUser(a.Config, f)
}

// Builds the HTTP and ActivityPub routes specific for this application.
//
// The database is provided so custom handlers can access application
// data directly, allowing clients to create the custom Fediverse
// behavior their application desires.
//
// The app.Framework provided allows handlers to use common behaviors
// provided by the apcore server framework.
//
// The bulk of typical HTTP application logic is in the handlers created
// by the app.Router. The apcore.app.Router also supports creating routes that
// process and serve ActivityStreams data, but the processing of the
// ActivityPub data itself is handled elsewhere in
// ApplyFederatingCallbacks and/or ApplySocialCallbacks.
func (a *Application) BuildRoutes(r app.Router, db app.Database, f app.Framework) error {
  // When building routes, the framework already provides actors at the
  // endpoint:
  //
  //     /users/{user}
  //
  // And further routes for the inbox, outbox, followers, following, and
  // liked collections. If you want to use web handlers at these
  // endpoints, other App interface functions allow you to do so.
  //
  // The framework also provides out-of-the-box OAuth2 supporting
  // endpoints:
  //
  //     /login (GET & POST)
  //     /logout (GET)
  //     /authorize (GET & POST)
  //     /token (GET)
  //
  // The framework also handles registering webfinger and host-meta
  // routes:
  //
  //     /.well-known/host-meta
  //     /.well-known/webfinger
  //
  // And supports using Webfinger to find actors on this server.

  // AP unrelated routes
  r.NewRoute().Path(
    fmt.Sprintf("%sapi/%s/config", a.Config.SocialtapConfig.SubFolder, apiVersion),
  ).Methods("GET").HandlerFunc(handlers.Config(a.Config, f))
  // registration endpoints
  r.NewRoute().Path(
    fmt.Sprintf("%sapi/%s/register", a.Config.SocialtapConfig.SubFolder, apiVersion),
  ).Methods("POST").HandlerFunc(handlers.Register(a.Scheme, a.Config, a, db, apiVersion))
  // reset user password
  r.NewRoute().Path(
    fmt.Sprintf("%sapi/%s/user/reset_password",
      a.Config.SocialtapConfig.SubFolder, apiVersion),
  ).Methods("POST").HandlerFunc(
    handlers.UserResetPasswordInstructions(a.Scheme, a.Config, db, apiVersion))
  // verify the cached token and take action
  r.NewRoute().Path(
    fmt.Sprintf("%sapi/%s/verify/{uuid}", a.Config.SocialtapConfig.SubFolder, apiVersion),
  ).Methods("GET").HandlerFunc(handlers.VerifyToken(a.Scheme, a.Config, a, db))
  // user information
  r.NewRoute().Path(
    fmt.Sprintf("%sapi/%s/userinfo/{uuid}", a.Config.SocialtapConfig.SubFolder, apiVersion),
  ).Methods("GET").HandlerFunc(handlers.UserInfo(a.Scheme, a.Config, a, f, db))
  // public and private search results
  r.NewRoute().Path(fmt.Sprintf(
    "%sapi/%s/search/{type}/{q}",
    a.Config.SocialtapConfig.SubFolder,
    apiVersion,
  )).Methods("GET").HandlerFunc(handlers.SearchResult(a.Scheme, a.Config, a, f, db))
  // public and private conversations
  r.NewRoute().Path(fmt.Sprintf(
    "%sapi/%s/conversation",
    a.Config.SocialtapConfig.SubFolder,
    apiVersion,
  )).Methods("GET").HandlerFunc(handlers.Conversation(f, db))
  // api untappd callback
  auth, _, err := untappd.NewAuthHandler(
    a.Config.SocialtapConfig.ClientID,
    a.Config.SocialtapConfig.ClientSecret,
    a.Config.SocialtapConfig.RedirectURL,
    handlers.Token(a.Scheme, a.Config, a, f, db),
    nil,
  ); if err != nil {
    panic(err.Error())
  }
  r.NewRoute().Path(
    fmt.Sprintf("%sapi/%s/callback", a.Config.SocialtapConfig.SubFolder, apiVersion),
  ).Methods("GET", "POST").HandlerFunc(auth.ServeHTTP)

  // ActivityPubHandleFunc is a convenience function for endpoints with
  // only ActivityPub content; no web content exists at this endpoint.
  r.ActivityPubOnlyHandleFunc(
    fmt.Sprintf("%sapi/ap/activities/{activity}", a.Config.SocialtapConfig.SubFolder),
    func(c util.Context, w http.ResponseWriter, r *http.Request, db app.Database) (permit bool, err error) {
    return true, nil
  })
  r.ActivityPubOnlyHandleFunc(
    fmt.Sprintf("%sapi/ap/accepts/{accept}", a.Config.SocialtapConfig.SubFolder),
    func(c util.Context, w http.ResponseWriter, r *http.Request, db app.Database) (permit bool, err error) {
    return true, nil
  })

  // Serialize and print different kind of vocabs
  r.NewRoute().Path(fmt.Sprintf("%sapi/ap/notes", a.Config.SocialtapConfig.SubFolder)).
    Methods("GET").HandlerFunc(
      handlers.VocabPage(a.Scheme, a.Config, f, db, streams.ActivityStreamsNoteName))
  r.NewRoute().Path(fmt.Sprintf("%sapi/ap/checkins", a.Config.SocialtapConfig.SubFolder)).
    Methods("GET").HandlerFunc(
      handlers.VocabPage(a.Scheme, a.Config, f, db, streams.SocialtapCheckinName))

  r.NewRoute().Path(fmt.Sprintf("%sapi/ap/notes/create", a.Config.SocialtapConfig.SubFolder)).
    Methods("POST").HandlerFunc(
      handlers.CreateVocab(a.Scheme, a.Config, a, f, streams.ActivityStreamsNoteName))
  r.NewRoute().Path(fmt.Sprintf("%sapi/ap/checkins/create", a.Config.SocialtapConfig.SubFolder)).
    Methods("POST").HandlerFunc(
      handlers.CreateVocab(a.Scheme, a.Config, a, f, streams.SocialtapCheckinName))

  // Display single types e.g. notes, drinks, etc. ..
  r.ActivityPubAndWebHandleFunc(
    fmt.Sprintf("%sapi/ap/notes/{note}", a.Config.SocialtapConfig.SubFolder),
    handlers.Auth(f, streams.ActivityStreamsNoteName), handlers.Vocab(a.Config, f))
  r.ActivityPubAndWebHandleFunc(
    fmt.Sprintf("%sapi/ap/checkins/{note}", a.Config.SocialtapConfig.SubFolder),
    handlers.Auth(f, streams.SocialtapCheckinName), handlers.Vocab(a.Config, f))
  //r.ActivityPubAndWebHandleFunc(
  //  fmt.Sprintf("%sapi/ap/drinks/{note}", a.Config.SocialtapConfig.SubFolder),
  //  authFn(streams.SocialtapDrinkName), handlerFn)
  //r.ActivityPubAndWebHandleFunc(
  //  fmt.Sprintf("%sapi/ap/venues/{note}", a.Config.SocialtapConfig.SubFolder),
  //  authFn(streams.SocialtapVenueName), handlerFn)
  //r.ActivityPubAndWebHandleFunc("/api/ap/breweries/{note}",
  //  authFn(streams.SocialtapManufacturerName), handlerFn)

  // Serve vueJS files via root
  fs := http.FileServer(http.Dir("dist"))
  r.PathPrefix(a.Config.SocialtapConfig.SubFolder).Handler(
    http.StripPrefix(a.Config.SocialtapConfig.SubFolder, fs))

  // Execute post function
  if a.PostBuildRoutes != nil {
    return a.PostBuildRoutes(r, db, f)
  }
  return nil
}

// NewIDPath creates a new id IRI path component for the content being
// created.
//
// A peer making a GET request to this path on this server should then
// serve the ActivityPub value provided in this call. For example:
//   "/notes/abcd0123-4567-890a-bcd0-1234567890ab"
//
// Ensure the route returned by NewIDPath will be servable by a handler
// created in the BuildRoutes call.
func (a Application) NewIDPath(c context.Context, t vocab.Type) (path string, err error) {
  switch t.GetTypeName() {
  case streams.SocialtapCheckinName:
    path = fmt.Sprintf("%sapi/ap/checkins/%s", a.Config.SocialtapConfig.SubFolder, uuid.New().String())
  case streams.ActivityStreamsNoteName:
    path = fmt.Sprintf("%sapi/ap/notes/%s",
      a.Config.SocialtapConfig.SubFolder, uuid.New().String())
  case streams.ActivityStreamsCreateName:
    path = fmt.Sprintf("%sapi/ap/activities/%s",
      a.Config.SocialtapConfig.SubFolder, uuid.New().String())
  case streams.ActivityStreamsAcceptName:
    path = fmt.Sprintf("%sapi/ap/accepts/%s",
      a.Config.SocialtapConfig.SubFolder, uuid.New().String())
  default:
    err = fmt.Errorf("NewID unhandled type name: %s", t.GetTypeName())
  }
  return
}

// ScopePermitsPrivateGetInbox determines if an OAuth token scope
// permits the bearer to view private (non-Public) messages in an
// actor's inbox.
func (Application) ScopePermitsPrivateGetInbox(scope string) (permitted bool, err error) {
  return scope == "getInbox" || scope == "all", nil
}

// ScopePermitsPrivateGetOutbox determines if an OAuth token scope
// permits the bearer to view private (non-Public) messages in an
// actor's outbox.
func (Application) ScopePermitsPrivateGetOutbox(scope string) (permitted bool, err error) {
  return scope == "getOutbox" || scope == "all", nil
}

// DefaultUserPreferences returns an application-specific preferences
// struct to be serialized into JSON and used as initial user app
// preferences.
func (Application) DefaultUserPreferences() interface{} {
  return nil
}

// DefaultUserPrivileges returns an application-specific privileges
// struct to be serialized into JSON and used as initial user app
// privileges.
func (Application) DefaultUserPrivileges() interface{} {
  return nil
}

// DefaultAdminPrivileges returns an application-specific privileges
// struct to be serialized into JSON and used as initial user app
// privileges for new admins.
func (Application) DefaultAdminPrivileges() interface{} {
  return nil
}

// Information about this application's software. This will be shown at
// the command line and used for NodeInfo statistics, as well as for
// user agent information.
func (Application) Software() app.Software {
  return app.Software{
    Name: "socialtap",
    UserAgent: "socialtap/1.0",
    MajorVersion: 1,
    MinorVersion: 0,
    PatchVersion: 1,
    Repository: "https://git.feneas.org/socialtap/server",
  }
}
