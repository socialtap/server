package badges
/*
 * Socialtap - Drink socially
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
  "fmt"

  "github.com/sams96/rgeo"
  "github.com/go-fed/activity/streams/vocab"
)

var CountryFn = func (badges *Badges, checkins []vocab.SocialtapCheckin) error {
  rev, err := rgeo.New(rgeo.Countries110)
  if err != nil {
    return err
  }
  // which manufacturer is from where? sort count by country name
  countries := make(map[string]int)
  checkinLoop:
  for _, checkin := range checkins {
    // XXX this is a workaround till the vocabulary was fixed
    // currently every location within a checkin receives as type venue or manufacturer
    // this is not serializable by gofed because it only accepts a place type
    m, err := checkin.Serialize()
    if err != nil {
      return err
    }

    manufacturers, ok := m["manufacturer"].(map[string]interface{}); if !ok {
      continue checkinLoop
    }
    locations, ok := manufacturers["location"].(map[string]interface{}); if !ok {
      continue checkinLoop
    }
    latitude, latOk := locations["latitude"].(float64)
    longitude, lngOk := locations["longitude"].(float64)
    if !(latOk && lngOk){
      continue checkinLoop
    }

    loc, err := rev.ReverseGeocode([]float64{
      longitude,
      latitude,
    }); if err != nil {
      continue checkinLoop
    }
    countries[loc.Country]++
  }

  for key, value := range countries {
    level := LevelByCount(value)
    badge := Badge{
      Name: key,
      Type: CountryBadge,
      Level: level,
      Description: fmt.Sprintf("You drank %d from %s (Level %d).", value, key, level),
    }
    *badges = append(*badges, badge)
  }
  return nil
}
