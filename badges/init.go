package badges
/*
 * Socialtap - Drink socially
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
  "math"

  "github.com/go-fed/activity/streams/vocab"
)

type BadgeType string

const (
  CountryBadge BadgeType = "Country"
)

type Badge struct {
  Name string `json:"name"`
  Type BadgeType `json:"type"`
  Description string `json:"description"`
  Level int `json:"level"`
  Callback func (*Badge, []vocab.SocialtapCheckin) bool `json:"-"`
}

type Badges []Badge

func LevelByCount(checkins int) int {
  if checkins <= 0 {
    return 1
  }
  return int(math.Log(float64(checkins)) * 5 + 1)
}

type listItem func (badges *Badges, checkins []vocab.SocialtapCheckin) error
type list []listItem

var List = list{
  CountryFn,
}
