/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package handlers

import (
  "fmt"
  "net/http"
  "encoding/json"
  "context"

  "github.com/google/uuid"
  "github.com/patrickmn/go-cache"
  "git.feneas.org/socialtap/server/database"
  "git.feneas.org/socialtap/server/workers"
  "github.com/go-fed/apcore/util"
  "github.com/go-fed/apcore/app"
  "github.com/go-fed/apcore/framework/config"
)

type registerUser struct {
  Email string `json:"email"`
  Username string `json:"username"`
  Password string `json:"password"`
  Repassword string `json:"repassword"`
}

func Register(scheme string, config *config.Config, a app.Application, db app.Database, apiVersion string) func(w http.ResponseWriter, r *http.Request) {

  return func(w http.ResponseWriter, r *http.Request) {
    var payload registerUser
    err := json.NewDecoder(r.Body).Decode(&payload)
    if err != nil {
      errorRequest(err, w, http.StatusBadRequest)
      return
    }

    if len(payload.Password) < 6 {
      errorRequest(
        fmt.Errorf("password must be at least six characters long"), w, http.StatusBadRequest)
      return
    }

    if payload.Password != payload.Repassword {
      errorRequest(
        fmt.Errorf("password and retyped password is not matching"), w, http.StatusBadRequest)
      return
    }

    if len(payload.Username) < 2 {
      errorRequest(
        fmt.Errorf("username must be at least two characters long"), w, http.StatusBadRequest)
      return
    }

    user, _ := database.FindApcoreUserByUsername(
      scheme, config, a, payload.Username)
    if user != nil && user.ID != "" {
      errorRequest(
        fmt.Errorf("username already exists"), w, http.StatusBadRequest)
      return
    }

    email, _ := database.FindUserByEmail(
      util.Context{context.Background()}, db, payload.Email)
    if email != "" {
      errorRequest(
        fmt.Errorf("email already exists"), w, http.StatusBadRequest)
      return
    }

    uuid := uuid.New().String()
    handlerCache.Set(uuid, &payload, cache.DefaultExpiration)

    go workers.MailWorker(
      config,
      payload.Email,
      "Confirm your email address",
      fmt.Sprintf(
        `To confirm your account visit: <a href="%s://%s/api/%s/verify/%s">Verify account</a>`,
        scheme, config.ServerConfig.Host, apiVersion, uuid,
      ))

    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(http.StatusOK)
    json.NewEncoder(w).Encode(struct{}{})
  }
}
