package handlers
/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
  "net/http"

  "github.com/go-fed/apcore/util"
  "github.com/go-fed/apcore/app"
  "git.feneas.org/socialtap/server/database"
)

func Auth(f app.Framework, vocabType string) (func(util.Context, http.ResponseWriter, *http.Request, app.Database) (bool, error)) {

  return func(c util.Context, w http.ResponseWriter, r *http.Request, db app.Database) (permit bool, err error) {
    // Determine who, if any, is logged-in.
    userID, authd, err := f.Validate(w, r)
    if err != nil {
      util.ErrorLogger.Errorf("error validating token/creds in GET /notes: %s", err)
      // continue processing request as unauthenticated.
    }
    ctx := f.Context(r)
    typeID, err := ctx.CompleteRequestURL()
    if err != nil {
      util.ErrorLogger.Errorf("error sending when creating note: %s", err)
      w.WriteHeader(http.StatusInternalServerError)
      return
    }
    if err == nil && authd {
      userIRI := f.UserIRI(userID)
      // Authenticated request
      permit, err = database.GetTypeIsReadable(ctx, db, typeID, userIRI, vocabType)
    } else {
      // Unauthenticated request
      permit, err = database.GetTypeIsPublic(ctx, db, typeID, vocabType)
    }
    return
  }
}
