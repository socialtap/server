/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package handlers

import (
  "fmt"
  "net/http"
  "encoding/json"
  "context"

  "github.com/google/uuid"
  "github.com/patrickmn/go-cache"
  "github.com/go-fed/apcore/util"
  "github.com/go-fed/apcore/paths"
  "git.feneas.org/socialtap/server/workers"
  "git.feneas.org/socialtap/server/database"
  "github.com/go-fed/apcore/app"
  "github.com/go-fed/apcore/framework/config"
)

type resetPassword struct {
  Email string `json:"email"`
  Password string `json:"password"`
  Repassword string `json:"repassword"`
  UserID paths.UUID `json:"-"`
}

func UserResetPasswordInstructions(scheme string, config *config.Config, db app.Database, apiVersion string) func(w http.ResponseWriter, r *http.Request) {

  return func(w http.ResponseWriter, r *http.Request) {
    var payload resetPassword
    err := json.NewDecoder(r.Body).Decode(&payload)
    if err != nil {
      errorRequestRedirect(
        err, config.SocialtapConfig.SubFolder, w, r, http.StatusInternalServerError)
      return
    }

    w.Header().Set("Content-Type", "application/json")
    userID, err := database.FindUserByEmail(
      util.Context{context.Background()}, db, payload.Email)
    if err != nil || userID == "" {
      // fail silently for security reasons
      w.WriteHeader(http.StatusOK)
      json.NewEncoder(w).Encode(struct{}{})
      return
    }
    payload.UserID = paths.UUID(userID)

    if len(payload.Password) < 6 {
      errorRequest(
        fmt.Errorf("password must be at least six characters long"), w, http.StatusBadRequest)
      return
    }

    if payload.Password != payload.Repassword {
      errorRequest(
        fmt.Errorf("password and retyped password is not matching"), w, http.StatusBadRequest)
      return
    }

    uuid := uuid.New().String()
    handlerCache.Set(uuid, &payload, cache.DefaultExpiration)

    go workers.MailWorker(
      config,
      payload.Email,
      "Confirm your password change request",
      fmt.Sprintf(
        `To confirm your new password visit: ` +
        `<a href="%s://%s/api/%s/verify/%s">Confirm password</a>`,
        scheme, config.ServerConfig.Host, apiVersion, uuid))

    w.WriteHeader(http.StatusOK)
    json.NewEncoder(w).Encode(struct{}{})
  }
}
