package handlers
/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
  "fmt"
  "time"
  "strings"
  "os"
  "net/url"
  "encoding/base64"

  "github.com/google/uuid"
  "github.com/patrickmn/go-cache"
  "github.com/go-fed/activity/streams/vocab"
  "github.com/go-fed/apcore/framework/config"
)

var handlerCache = cache.New(15 * time.Minute, 20 * time.Minute)

func RelativeIriPath(config *config.Config, vocabType vocab.Type) (result string) {
  iri := vocabType.GetJSONLDId().GetIRI()
  //result = strings.TrimPrefix(iri.String(), fmt.Sprintf(
  //  "%s://%s%s", a.Scheme, config.ServerConfig.Host, config.SocialtapConfig.SubFolder))
  // XXX fix scheme
  result = strings.TrimPrefix(iri.String(), fmt.Sprintf(
    "http://%s%s", config.ServerConfig.Host, config.SocialtapConfig.SubFolder))
  result = strings.TrimPrefix(iri.String(), fmt.Sprintf(
    "https://%s%s", config.ServerConfig.Host, config.SocialtapConfig.SubFolder))
  result = strings.TrimPrefix(result, "api/ap")
  return
}

func saveDataURLToDisk(scheme string, config *config.Config, dataURL string) (*url.URL, error) {
  // A typical data url looks like the following
  //    data:image/<ext>;base64,<base64>
  dataURLParts := strings.Split(dataURL, ",")
  b, err := base64.StdEncoding.DecodeString(strings.Join(dataURLParts[1:], ""))
  if err != nil {
    return nil, err
  }

  var megaByte = 1 << 20
  // XXX needs to be configurable
  var limit = 3 * megaByte
  if len(b) / megaByte > limit {
    return nil, fmt.Errorf("the data url exceeded the limit of %d bytes", limit)
  }

  // build upload path
  name := uuid.New().String()
  year, month, day := time.Now().UTC().Date()
  uploadPath := fmt.Sprintf("dist/uploads/%d/%d/%d", year, month, day)
  err = os.MkdirAll(uploadPath, 0700)
  if err != nil {
    return nil, err
  }

  f, err := os.Create(fmt.Sprintf("%s/%s", uploadPath, name))
  if err != nil {
    return nil, err
  }
  defer f.Close()

  _, err = f.Write(b)
  if err != nil {
    return nil, err
  }

  return &url.URL{
    Scheme: scheme,
    Host: config.ServerConfig.Host,
    Path: fmt.Sprintf("/uploads/%d/%d/%d/%s", year, month, day, name),
  }, f.Sync()
}
