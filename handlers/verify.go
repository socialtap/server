/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package handlers

import (
  "fmt"
  "net/http"
  "context"

  "github.com/gorilla/mux"
  "git.feneas.org/socialtap/server/database"
  "git.feneas.org/socialtap/server/crypto"
  "github.com/go-fed/apcore/util"
  "github.com/go-fed/apcore/app"
  "github.com/go-fed/apcore/framework/config"
)

func VerifyToken(scheme string, config *config.Config, a app.Application, db app.Database) func(w http.ResponseWriter, r *http.Request) {
  return func(w http.ResponseWriter, r *http.Request) {
    uuid := mux.Vars(r)["uuid"]
    defer func() {
      handlerCache.Delete(uuid)
    }()

    tokenI, _ := handlerCache.Get(uuid)
    switch token := tokenI.(type) {
    case *registerUser:
      _, err := database.CreateUser(
        scheme, config, a, token.Username, token.Email, token.Password)
      if err != nil {
        errorRequestRedirect(
          err, config.SocialtapConfig.SubFolder, w, r, http.StatusInternalServerError)
        return
      }
    case *resetPassword:
      params := crypto.BcryptParams{
        SaltSize: config.ServerConfig.SaltSize,
        BCryptStrength: config.ServerConfig.BCryptStrength,
      }

      salt, hash, err := crypto.Bcrypt(params, token.Password)
      if err != nil {
        errorRequestRedirect(
          err, config.SocialtapConfig.SubFolder, w, r, http.StatusInternalServerError)
        return
      }

      err = database.UpdateUserPasswordAndSalt(
        util.Context{context.Background()}, db, token.UserID, hash, salt)
      if err != nil {
        errorRequestRedirect(
          err, config.SocialtapConfig.SubFolder, w, r, http.StatusInternalServerError)
        return
      }
    default:
      errorRequestRedirect(
        fmt.Errorf("token does not exist"),
        config.SocialtapConfig.SubFolder, w, r, http.StatusBadRequest)
      return
    }
    http.Redirect(w, r, fmt.Sprintf("%s#/login", config.SocialtapConfig.SubFolder), 301)
  }
}
