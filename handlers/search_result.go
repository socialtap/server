package handlers
/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
  "net/http"
  "strings"
  "encoding/json"

  "github.com/gorilla/mux"
  "git.feneas.org/socialtap/server/workers"
  "git.feneas.org/socialtap/server/database"
  "github.com/go-fed/apcore/util"
  "github.com/go-fed/activity/streams"
  "github.com/go-fed/activity/streams/vocab"
  "github.com/go-fed/apcore/framework/config"
  "github.com/go-fed/apcore/app"
)

func SearchResult(scheme string, config *config.Config, a app.Application, f app.Framework, db app.Database) func(http.ResponseWriter, *http.Request) {

  return func(w http.ResponseWriter, r *http.Request) {
    vocabType := strings.Title(mux.Vars(r)["type"])
    search := mux.Vars(r)["q"]

    userID, authd, err := f.Validate(w, r)
    if err != nil {
      util.ErrorLogger.Errorf("error validating token/creds in GET /notes: %s", err)
      // continue processing request as unauthenticated.
    }

    if vocabType == "Untappd" {
      if err != nil || !authd {
        util.ErrorLogger.Errorf("untappd search requires authentication")
        w.WriteHeader(http.StatusForbidden)
        return
      }

      beers, err := workers.UntappdSearchBeerWorker(config, scheme, a, db, userID, search)
      if err != nil {
        util.ErrorLogger.Errorf("error getting notes: %s", err)
        w.WriteHeader(http.StatusInternalServerError)
        return
      }
      w.Header().Set("Content-Type", "application/json")
      json.NewEncoder(w).Encode(beers)
    } else {
      var types []vocab.Type
      if err != nil || !authd {
        types, err = database.GetPublicSearchResults(r.Context(), db, vocabType, search)
      } else {
        userIRI := f.UserIRI(userID)
        types, err = database.GetPublicAndPrivateSearchResults(
          r.Context(), db, vocabType, userIRI, search)
      }
      if err != nil {
        util.ErrorLogger.Errorf("error getting notes: %s", err)
        w.WriteHeader(http.StatusInternalServerError)
        return
      }

      var serializedTypes []map[string]interface{}
      for _, t := range types {
        serialized, err := streams.Serialize(t)
        if err != nil {
          util.ErrorLogger.Errorf("cannot serialize notes: %s", err)
          w.WriteHeader(http.StatusInternalServerError)
          return
        }
        serializedTypes = append(serializedTypes, serialized)
      }
      w.Header().Set("Content-Type", "application/json")
      json.NewEncoder(w).Encode(serializedTypes)
    }
  }
}
