/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package handlers

import (
  "net/http"
  "encoding/json"

  "github.com/go-fed/apcore/framework/config"
  "github.com/go-fed/apcore/app"
)

func Config(config *config.Config, f app.Framework) func(w http.ResponseWriter, r *http.Request) {

  return func(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")

    uuid, authd, err := f.Validate(w, r)
    userID := string(uuid)
    config := struct {
      SubFolder string `json:"subFolder"`
      ClientID string `json:"clientID"`
      Host string `json:"host"`
      RedirectURL string `json:"redirectURL"`
      Debug bool `json:"debug"`
      Worker bool `json:"worker"`
      Mail bool `json:"mail"`
      Authenticated bool `json:"authenticated"`
      UserID *string `json:"userID,omitempty"`
    }{
      SubFolder: config.SocialtapConfig.SubFolder,
      ClientID: config.SocialtapConfig.ClientID,
      Host: config.ServerConfig.Host,
      RedirectURL: config.SocialtapConfig.RedirectURL,
      Debug: config.SocialtapConfig.Debug,
      Worker: config.SocialtapConfig.Worker,
      Mail: config.SocialtapConfig.Mail,
      Authenticated: err == nil && authd && userID != "",
    }

    if config.Authenticated {
      config.UserID = &userID
    }
    json.NewEncoder(w).Encode(config)
  }
}
