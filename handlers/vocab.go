package handlers
/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
  "fmt"
  "net/http"
  "strconv"
  "encoding/json"
  "strings"

  "github.com/go-fed/apcore/util"
  "github.com/go-fed/apcore/paths"
  "github.com/go-fed/activity/streams"
  "github.com/go-fed/activity/streams/vocab"
  "git.feneas.org/socialtap/server/database"
  "git.feneas.org/socialtap/server/workers"
  "github.com/go-fed/apcore/app"
  "github.com/go-fed/apcore/framework/config"
  "github.com/go-fed/activity/pub"
  builder "git.feneas.org/socialtap/server/vocab_builder"
)

func VocabPage(scheme string, config *config.Config, f app.Framework, db app.Database, vocabType string) func(http.ResponseWriter, *http.Request) {

  return func(w http.ResponseWriter, r *http.Request) {
    var (
      queryPage int = 1
      queryUUID *paths.UUID
      err error
    )
    if requestQueryPage, ok := r.URL.Query()["page"]; ok {
      queryPage, err = strconv.Atoi(requestQueryPage[0])
      if err != nil {
        queryPage = 1
      }
    }
    if requestQueryUUID, ok := r.URL.Query()["uuid"]; ok {
      uuid := paths.UUID(requestQueryUUID[0])
      queryUUID = &uuid
    }

    userID, authd, err := f.Validate(w, r)
    if err != nil {
      util.ErrorLogger.Errorf("error validating token/creds in GET /notes: %s", err)
      // continue processing request as unauthenticated.
    }
    var notes []vocab.Type
    if queryUUID != nil {
      userIRI := f.UserIRI(*queryUUID)
      notes, err = database.GetPersonPublicTypes(
        r.Context(), db, queryPage, userIRI.String(), vocabType)
    } else if err != nil || !authd {
      notes, err = database.GetLatestPublicTypes(r.Context(), db, queryPage, vocabType)
    } else {
      userIRI := f.UserIRI(userID)
      notes, err = database.GetLatestPublicAndPrivateTypes(
        r.Context(), db, queryPage, userIRI.String(), vocabType)
    }
    if err != nil {
      util.ErrorLogger.Errorf("error getting notes: %s", err)
      w.WriteHeader(http.StatusInternalServerError)
      return
    }

    w.Header().Set("Content-Type", "application/activity+json")
    page := streams.NewActivityStreamsOrderedCollectionPage()
    obj := streams.NewActivityStreamsOrderedItemsProperty()
    for _, item := range notes {
      switch entity := item.(type) {
      case vocab.ActivityStreamsNote:
        obj.AppendActivityStreamsNote(entity)
      case vocab.SocialtapCheckin:
        obj.AppendSocialtapCheckin(entity)
      default:
        util.ErrorLogger.Errorf("unsupported vocab type: %s", vocabType)
        w.WriteHeader(http.StatusInternalServerError)
        return
      }
    }
    page.SetActivityStreamsOrderedItems(obj)
    pageId := streams.NewJSONLDIdProperty()
    r.URL.Scheme = scheme
    r.URL.Host = config.ServerConfig.Host
    // Add Next and Prev property if it is necessary
    // XXX ten is hard-coded in activity_services.go (LIMIT 10)
    if len(notes) == 10 {
      nextUrl := *r.URL
      nextUrl.RawQuery = fmt.Sprintf("page=%d", queryPage + 1)
      next := streams.NewActivityStreamsNextProperty()
      next.SetIRI(&nextUrl)
      page.SetActivityStreamsNext(next)
    }
    if queryPage > 1 {
      prevUrl := *r.URL
      prevUrl.RawQuery = fmt.Sprintf("page=%d", queryPage - 1)
      prev := streams.NewActivityStreamsPrevProperty()
      prev.SetIRI(&prevUrl)
      page.SetActivityStreamsPrev(prev)
    }
    pageIdIRI := *r.URL
    pageIdIRI.RawQuery = fmt.Sprintf("page=%d", queryPage)
    pageId.SetIRI(&pageIdIRI)
    page.SetJSONLDId(pageId)

    serialized, err := streams.Serialize(page)
    if err != nil {
      util.ErrorLogger.Errorf("cannot serialize page: %s", err)
      w.WriteHeader(http.StatusInternalServerError)
      return
    }
    json.NewEncoder(w).Encode(serialized)
  }
}

func CreateVocab(scheme string, config *config.Config, a app.Application, f app.Framework, vocabType string) func(http.ResponseWriter, *http.Request) {

  return func(w http.ResponseWriter, r *http.Request) {
    // ensure the user is logged in.
    userID, authd, err := f.Validate(w, r)
    if err != nil {
      util.ErrorLogger.Errorf("error validating oauth2 token in POST /notes/create: %s", err)
      w.WriteHeader(http.StatusInternalServerError)
      return
    }
    if !authd {
      http.Redirect(w, r, config.SocialtapConfig.SubFolder, http.StatusFound)
      return
    }

    var params map[string]interface{}
    err = json.NewDecoder(r.Body).Decode(&params)
    if err != nil {
      w.WriteHeader(http.StatusBadRequest)
      util.ErrorLogger.Errorf("cannot decode json: %s", err)
      return
    }

    // check if we have to store image data on disk
    mediaKeys := []string{"checkinMedia", "drinkMedia", "manufacturerMedia", "venueMedia"}
    for _, key := range mediaKeys {
      media, ok := params[key].(string); if !ok {
        continue
      }

      if !strings.HasPrefix(media, "http") {
        iri, err := saveDataURLToDisk(scheme, config, media)
        if err != nil {
          w.WriteHeader(http.StatusBadRequest)
          util.ErrorLogger.Errorf("cannot save data url to disk: %s", err)
          return
        }
        // use new location
        params[key] = iri.String()
      }
    }

    type toCcType interface {
      vocab.Type
      GetActivityStreamsTo() vocab.ActivityStreamsToProperty
      SetActivityStreamsCc(vocab.ActivityStreamsCcProperty)
    }

    var entity toCcType
    switch vocabType {
    case streams.ActivityStreamsNoteName:
      entity, err = builder.Note(params)
      if err != nil {
        util.ErrorLogger.Errorf(
          "error while building note type in POST /notes/create: %s", err)
        w.WriteHeader(http.StatusInternalServerError)
        return
      }
    case streams.SocialtapCheckinName:
      // send it to untappd too. Only works if the user
      // authenticated with untappd
      if bid, ok := params["untappdBeerID"].(float64); ok {
        go workers.UntappdSendCheckinWorker(config, scheme, f, a, userID, int(bid), params)
      }

      entity, err = builder.Checkin(params)
      if err != nil {
        util.ErrorLogger.Errorf(
          "error while building manufacturer type in POST /breweries/create: %s", err)
        w.WriteHeader(http.StatusInternalServerError)
        return
      }
    default:
      w.WriteHeader(http.StatusBadRequest)
      return
    }

    to := entity.GetActivityStreamsTo()
    toLoop:
    for iter := to.Begin(); iter != to.End(); iter = iter.Next() {
      if iter.GetIRI().String() == pub.PublicActivityPubIRI {
        cc := streams.NewActivityStreamsCcProperty()
        cc.AppendIRI(paths.UUIDIRIFor(
          scheme, config.ServerConfig.Host, paths.FollowersPathKey, userID))
        entity.SetActivityStreamsCc(cc)
        break toLoop
      }
    }

    ctx := util.Context{r.Context()}
    // Send the note -- a Create will automatically be created
    if err := f.Send(ctx, paths.UUID(userID), entity); err != nil {
      util.ErrorLogger.Errorf("error sending when creating note: %s", err)
      w.WriteHeader(http.StatusInternalServerError)
      return
    }
    iri := entity.GetJSONLDId().GetIRI()
    http.Redirect(w, r, iri.String(), http.StatusFound)
  }
}

func Vocab(config *config.Config, f app.Framework) func(http.ResponseWriter, *http.Request) {

  return func(w http.ResponseWriter, r *http.Request) {
    ctx := f.Context(r)
    typeID, err := ctx.CompleteRequestURL()
    if err != nil {
      util.ErrorLogger.Errorf("error sending when creating note: %s", err)
      w.WriteHeader(http.StatusInternalServerError)
      return
    }
    vt, err := f.GetByIRI(ctx, typeID)
    if err != nil {
      util.ErrorLogger.Errorf("error fetching note: %s", err)
      w.WriteHeader(http.StatusInternalServerError)
      return
    }

    if r.Header.Get("Content-Type") == "application/activity+json" {
      w.Header().Set("Content-Type", "application/activity+json")
      json.NewEncoder(w).Encode(vt)
    } else {
      http.Redirect(w, r, fmt.Sprintf(
        "%s#/%s", config.SocialtapConfig.SubFolder, RelativeIriPath(config, vt),
      ), http.StatusFound)
    }
  }
}
