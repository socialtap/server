/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package handlers

import (
  "fmt"
  "net/http"
  "encoding/json"

  "github.com/go-fed/apcore/util"
  "git.feneas.org/socialtap/server/workers"
  "git.feneas.org/socialtap/server/database"
  "github.com/go-fed/apcore/app"
  "github.com/go-fed/apcore/framework/config"
)

func Token(scheme string, config *config.Config, a app.Application, f app.Framework, db app.Database) func(token string, w http.ResponseWriter, r *http.Request) {

  return func(token string, w http.ResponseWriter, r *http.Request) {
    // check if user is authenticated
    userID, authd, err := f.Validate(w, r)
    if err != nil || !authd {
      errorRequestRedirect(
        err, config.SocialtapConfig.SubFolder, w, r, http.StatusForbidden)
      return
    }

    prefs, err := database.FindUserPreferences(scheme, config, a, userID)
    if err != nil {
      errorRequestRedirect(
        err, config.SocialtapConfig.SubFolder, w, r, http.StatusInternalServerError)
      return
    }

    var prefMap map[string]interface{}
    prefBytes, err := json.Marshal(prefs.AppPreferences)
    if err != nil {
      errorRequestRedirect(
        err, config.SocialtapConfig.SubFolder, w, r, http.StatusInternalServerError)
      return
    }
    err = json.Unmarshal(prefBytes, &prefMap)
    if err != nil {
      errorRequestRedirect(
        err, config.SocialtapConfig.SubFolder, w, r, http.StatusInternalServerError)
      return
    }
    // in case prefBytes equals null
    if prefMap == nil {
      prefMap = make(map[string]interface{})
    }

    _, tokenExists := prefMap["UntappdToken"]

    prefMap["UntappdToken"] = token
    prefs.AppPreferences = prefMap

    err = database.UpdateUserPreferences(scheme, config, a, string(userID), prefs)
    if err != nil {
      errorRequestRedirect(
        err, config.SocialtapConfig.SubFolder, w, r, http.StatusInternalServerError)
      return
    }

    if !tokenExists {
      // first-time setup
      go func() {
        err = workers.UntappdCheckinWorker(config, scheme, f, a, db, userID)
        if err != nil {
          util.ErrorLogger.Errorln("cannot fetch checkins:", err)
        }
      }()
    }
    http.Redirect(w, r, fmt.Sprintf("%s#/settings", config.SocialtapConfig.SubFolder), 301)
    return
  }
}
