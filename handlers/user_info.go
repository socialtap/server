/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package handlers

import (
  "fmt"
  "net/http"
  "net/url"
  "encoding/json"
  "context"

  "github.com/patrickmn/go-cache"
  "github.com/gorilla/mux"
  "github.com/go-fed/apcore/util"
  "git.feneas.org/socialtap/server/database"
  "git.feneas.org/socialtap/server/badges"
  "github.com/go-fed/apcore/app"
  "github.com/go-fed/apcore/framework/config"
)

type info struct{
  TotalCheckins int `json:"totalCheckins"`
  UniqueCheckins int `json:"uniqueCheckins"`
  UntappdService *bool `json:"untappdService,omitempty"`
  UntappdSynced *bool `json:"untappdSynced,omitempty"`
  Badges *badges.Badges `json:"badges,omitempty"`
}

func UserInfo(scheme string, config *config.Config, a app.Application, f app.Framework, db app.Database) func(w http.ResponseWriter, r *http.Request) {

  rootIRI := fmt.Sprintf("%s://%s%susers/",
    scheme,
    config.ServerConfig.Host,
    config.SocialtapConfig.SubFolder,
  )
  return func(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")

    ctx := util.Context{context.Background()}
    uuid := mux.Vars(r)["uuid"]
    iri, err := url.Parse(fmt.Sprintf("%s%s", rootIRI, uuid))
    if err != nil {
      errorRequest(err, w, http.StatusBadRequest)
      return
    }

    checkins, err := database.TotalCheckinsByIRI(ctx, db, iri)
    if err != nil {
      errorRequest(err, w, http.StatusInternalServerError)
      return
    }

    unique, err := database.UniqueCheckinCounterByIRI(ctx, db, iri)
    if err != nil {
      errorRequest(err, w, http.StatusInternalServerError)
      return
    }

    info := info{
      TotalCheckins: len(checkins),
      UniqueCheckins: unique,
    }

    // check if the user is authenticated
    userID, authd, err := f.Validate(w, r)
    if err == nil && authd {
      var untappd, synced bool
      prefs, err := database.FindUserPreferences(scheme, config, a, userID)
      if err == nil {
        var prefMap map[string]interface{}
        prefBytes, err := json.Marshal(prefs.AppPreferences)
        if err == nil {
          err = json.Unmarshal(prefBytes, &prefMap)
          if err == nil {
            _, untappd = prefMap["UntappdToken"]
            _, synced = prefMap["UntappdInitSync"]
          }
        }
      }
      info.UntappdService = &untappd
      info.UntappdSynced = &synced
    }

    var items badges.Badges
    cacheKey := fmt.Sprintf("%s#UserInfo", iri.String())
    cachedBadges, _ := handlerCache.Get(cacheKey)

    if b, ok := cachedBadges.(*badges.Badges); ok && b != nil {
      items = *b
    } else {
      for _, cb := range badges.List {
        err := cb(&items, checkins)
        if err != nil {
          util.InfoLogger.Infof("cannot execute badge function: %s", err)
        }
      }
    }

    if len(items) > 0 {
      info.Badges = &items
    }
    // set cache before encoding
    handlerCache.Set(cacheKey, &info, cache.DefaultExpiration)

    json.NewEncoder(w).Encode(info)
  }
}
