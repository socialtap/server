package handlers
/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
  "net/http"
  "net/url"
  "encoding/json"

  "github.com/go-fed/apcore/app"
  "github.com/go-fed/apcore/util"
  "github.com/go-fed/activity/streams"
  "github.com/go-fed/activity/streams/vocab"
  "git.feneas.org/socialtap/server/database"
)

func Conversation(f app.Framework, db app.Database) func(http.ResponseWriter, *http.Request) {

  return func(w http.ResponseWriter, r *http.Request) {
    queryParam, ok := r.URL.Query()["q"]; if !ok {
      util.ErrorLogger.Errorf("no conversation iri specified")
      w.WriteHeader(http.StatusBadRequest)
      return
    }

    convIri, err := url.Parse(queryParam[0])
    if err != nil {
      util.ErrorLogger.Errorf("cannot parse conversation iri: %s", err)
      w.WriteHeader(http.StatusBadRequest)
      return
    }

    userID, authd, err := f.Validate(w, r)
    if err != nil {
      util.ErrorLogger.Errorf("error validating token/creds in GET /notes: %s", err)
      // continue processing request as unauthenticated.
    }

    var notes []vocab.Type
    if err != nil || !authd {
      notes, err = database.GetPublicConversation(r.Context(), db, convIri)
    } else {
      userIRI := f.UserIRI(userID)
      notes, err = database.GetPublicOrPrivateConversation(
        r.Context(), db, convIri, userIRI)
    }
    if err != nil {
      util.ErrorLogger.Errorf("error getting notes: %s", err)
      w.WriteHeader(http.StatusInternalServerError)
      return
    }

    var serializedNotes []map[string]interface{}
    for _, note := range notes {
      serialized, err := streams.Serialize(note)
      if err != nil {
        util.ErrorLogger.Errorf("cannot serialize notes: %s", err)
        w.WriteHeader(http.StatusInternalServerError)
        return
      }
      serializedNotes = append(serializedNotes, serialized)
    }
    w.Header().Set("Content-Type", "application/json")
    json.NewEncoder(w).Encode(serializedNotes)
  }
}
