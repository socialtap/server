/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package handlers

import (
  "fmt"
  "net/http"
  "encoding/json"

  "github.com/go-fed/apcore/util"
)

func errorRequest(err error, w http.ResponseWriter, status int) {
  w.Header().Set("Content-Type", "application/json")
  util.ErrorLogger.Errorln(err)
  w.WriteHeader(status)
  json.NewEncoder(w).Encode(struct{}{})
}

func errorRequestRedirect(err error, subfolder string, w http.ResponseWriter, r *http.Request, status int) {
  util.ErrorLogger.Errorln(err)
  http.Redirect(w, r, fmt.Sprintf("%s#/error/%d", subfolder, status), 301)
}
