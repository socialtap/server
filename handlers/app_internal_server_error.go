/*
 * Socialtap - Drink socially
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package handlers

import (
  "fmt"
  "net/http"

  "github.com/go-fed/apcore/framework/config"
  "github.com/go-fed/apcore/app"
)

func AppInternalServerError(config *config.Config, f app.Framework) http.Handler {
  return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
    http.Redirect(w, r,
      fmt.Sprintf("%s#/error", config.SocialtapConfig.SubFolder),
      http.StatusInternalServerError,
    )
  })
}
