/*
 * Socialtap - Drink socially
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package handlers

import (
  "fmt"
  "net/http"

  "github.com/go-fed/apcore/util"
  "github.com/go-fed/activity/streams/vocab"
  "github.com/go-fed/apcore/framework/config"
  "github.com/go-fed/apcore/app"
)

func AppUser(config *config.Config, f app.Framework) (app.VocabHandlerFunc, app.AuthorizeFunc) {

  return func(w http.ResponseWriter, r *http.Request, vocabType vocab.Type) {
    http.Redirect(w, r,
      fmt.Sprintf(
        "%s#/%s", config.SocialtapConfig.SubFolder, RelativeIriPath(config, vocabType)),
      http.StatusFound,
    )
  }, func(c util.Context, w http.ResponseWriter, r *http.Request, db app.Database) (permit bool, err error) {
    // XXX make that optional in future
    return true, nil
  }
}
