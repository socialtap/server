import axios from 'axios'

const baseURL = '/'
const baseURLSt = `${baseURL}api/v0`
const baseURLAp = `${baseURL}api/ap`
const nominatimURL = 'https://nominatim.openstreetmap.org'
const contentType = 'application/json'
const contentTypeAp = 'application/activity+json'
const headers = {
  'Content-Type': contentType,
  'Accept': [
    contentType,
    contentTypeAp
  ]
}

export const Base = axios.create({
  baseURL: baseURL,
  headers: headers
})

export const Socialtap = axios.create({
  baseURL: baseURLSt,
  headers: headers
})

export const ActivityPub = axios.create({
  baseURL: baseURLAp,
  headers: headers
})

export const Nominatim = axios.create({
  baseURL: nominatimURL,
  headers: headers
})

export const WebfingerFuture = (username, host) => {
  return axios.create({ baseURL: baseURL, headers: headers })
    .get(`/.well-known/webfinger?resource=acct:${username}@${host}`)
}

export const PersonFuture = (webfinger) => {
  for (var idx in webfinger.links) {
    var link = webfinger.links[idx]
    if (link.rel === 'self' && link.type === contentTypeAp) {
      return Base.get(link.href)
    }
  }
  return null
}

export const onError = (ctx, error) => {
  if (typeof error.response === 'undefined') {
    console.debug(error)
    return
  } else {
    console.debug(error.response)
  }

  switch (error.response.status) {
    case 403:
      ctx.$root.apiConfig.authenticated = false
      // kill all running intervals
      ctx.$root.$emit('interval', -1)
      break
    case 400:
      switch (error.response.config.url) {
        case '/register':
          ctx.$root.$emit(
            'toast',
            'Registration failed',
            'Ensure that your username has more the two characters and your passwords are matching',
            8000,
            'warning'
          )
          break
        default:
          ctx.$root.$emit(
            'toast',
            'Unexpected error',
            'For more details check the console log',
            undefined,
            'danger'
          )
      }
      break
    default:
      ctx.$root.$emit(
        'toast',
        'Unexpected error',
        'For more details check the console log',
        undefined,
        'danger'
      )
  }
}
