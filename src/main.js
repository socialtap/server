import Vue from 'vue'
import App from '@/App.vue'
import router from '@/router'
import { Socialtap, onError } from '@/api'
import moment from 'moment'
import VueGeolocation from 'vue-browser-geolocation'

// font awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faBars, faSync, faBeer, faSignInAlt, faSignOutAlt, faUserPlus,
  faExclamationTriangle, faUser, faCogs, faCheck, faAlignLeft,
  faReply, faStream, faHeart, faChevronDown,
  // rich text editor
  faBold, faItalic, faStrikethrough, faUnderline, faCode, faParagraph,
  faHeading, faListUl, faListOl, faQuoteRight, faTerminal, faRulerHorizontal,
  faUndo, faRedo
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
// import 'vue-material/dist/theme/default.css'
import '@/assets/stylesheets/md-theme.scss'
import '@/assets/stylesheets/md-color.scss'

Vue.config.productionTip = false

// vue material
Vue.use(VueMaterial)

Vue.use(VueGeolocation);

// font awesome
library.add(
  faBars, faSync, faBeer, faSignInAlt, faSignOutAlt, faUserPlus,
  faExclamationTriangle, faUser, faCogs, faCheck, faAlignLeft,
  faReply, faStream, faHeart, faChevronDown,
  // rich text editor
  faBold, faItalic, faStrikethrough, faUnderline, faCode, faParagraph,
  faHeading, faListUl, faListOl, faQuoteRight, faTerminal, faRulerHorizontal,
  faUndo, faRedo
)
Vue.component('font-awesome-icon', FontAwesomeIcon)

// register momentjs as prototype variable
Vue.prototype.moment = moment

Socialtap.get('/config').then(response => {
  new Vue({
    router,
    data () {
      return {
        userInfo: {},
        apiConfig: response.data
      }
    },
    mounted () {
      this.$root.$on('update-userinfo', () => {
        Socialtap.get(`/userinfo/${this.apiConfig.userID}`).then((resp) => {
          this.userInfo = resp.data
        }).catch((resp) => onError(this, resp))
      })
      this.$root.$emit('update-userinfo')
    },
    render: h => h(App),
  }).$mount('#app')
}).catch(err => console.error(err))
