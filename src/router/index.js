import Vue from 'vue'
import Router from 'vue-router'

import Index from '@/components/Index'
import Tap from '@/components/Tap'
import Stream from '@/components/Stream'
import CreateCheckin from '@/components/CreateCheckin'
import Conversation from '@/components/Conversation'
import Login from '@/components/Login'
import Register from '@/components/Register'
import User from '@/components/User'
import Setting from '@/components/Setting'
import Error from '@/components/Error'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/tap',
      name: 'Tap',
      component: Tap
    },
    {
      path: '/create',
      name: 'CreateCheckin',
      component: CreateCheckin,
      props: true
    },
    {
      path: '/stream',
      name: 'Stream',
      component: Stream
    },
    {
      path: '/conversation/:q',
      name: 'Conversation',
      component: Conversation
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/users/:uuid',
      name: 'User',
      component: User
    },
    {
      path: '/settings',
      name: 'Setting',
      component: Setting
    },
    {
      path: '/error/:code',
      name: 'Error',
      component: Error
    }
  ],
  linkActiveClass: '',
  linkExactActiveClass: 'active'
})
