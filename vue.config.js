module.exports = {
  devServer: {
    port: 8081,
    writeToDisk: true
  },
  chainWebpack: config => {
    config
      .plugin('html')
      .tap(args => {
        args[0].title = 'Socialtap - drink socially'
        return args
      })
  }
}
