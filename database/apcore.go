// apcore is a server framework for implementing an ActivityPub application.
// Copyright (C) 2019 Cory Slep
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package database

import (
  "database/sql"
  "math/rand"
  "context"
  "time"

  "github.com/go-fed/activity/pub"

  "github.com/go-fed/apcore/ap"
  "github.com/go-fed/apcore/app"
  "github.com/go-fed/apcore/models"
  "github.com/go-fed/apcore/util"
  "github.com/go-fed/apcore/services"
  "github.com/go-fed/apcore/framework/db"
  "github.com/go-fed/apcore/framework/config"
)

func InitApcoreDatabaseSetup(scheme string, config *config.Config, a app.Application) {
  err := createTables(scheme, config, a)
  if err != nil {
    util.ErrorLogger.Fatal(err)
  }
  err = createInstanceActor(scheme, config, a)
  if err != nil {
    // this will return an error if the user was created before
    util.ErrorLogger.Warning(err)
  }
  err = createServerPreferences(scheme, config, a)
  if err != nil {
    util.ErrorLogger.Fatal(err)
  }
}

func createServerPreferences(scheme string, config *config.Config, a app.Application) error {
  db, _, users, _, _ := NewModels(scheme, config, a)
  defer db.Close()

  tx, err := db.BeginTx(context.Background(), nil)
  if err != nil {
    return err
  }
  defer tx.Rollback()

  sp := services.ServerPreferences{
    OnFollow: pub.OnFollowAutomaticallyAccept,
    OpenRegistrations: true,
    ServerBaseURL: config.ServerConfig.Host,
    ServerName: config.ServerConfig.Host,
    OrgName: "",
    OrgContact: "",
    OrgAccount: "",
  }

  err = users.SetServerPreferences(util.Context{context.Background()}, sp)
  if err != nil {
    return err
  }
  return tx.Commit()
}

func createInstanceActor(scheme string, config *config.Config, a app.Application) error {
  db, _, users, _, _ := NewModels(scheme, config, a)
  defer db.Close()

  tx, err := db.BeginTx(context.Background(), nil)
  if err != nil {
    return err
  }
  defer tx.Rollback()

  _, err = users.CreateInstanceActorSingleton(
    util.Context{context.Background()},
    scheme,
    config.ServerConfig.Host,
    config.ServerConfig.RSAKeySize,
  ); if err != nil {
    return err
  }
  return tx.Commit()
}

func createTables(scheme string, config *config.Config, a app.Application) error {
  db, dialect, err := db.NewDB(config)
  if err != nil {
    util.ErrorLogger.Fatal(err)
  }

  clock, err := ap.NewClock(config.ActivityPubConfig.ClockTimezone)
  if err != nil {
    util.ErrorLogger.Fatal(err)
  }

  _, _, _, _, _, _, _, _, _, _, _, _, _, _, ml := createModelsAndServices(
    config, db, dialect, a, config.ServerConfig.Host, scheme, clock,
  )

  tx, err := db.BeginTx(context.Background(), nil)
  if err != nil {
    return err
  }
  defer tx.Rollback()
  for _, m := range ml {
    if err := m.CreateTable(tx, dialect); err != nil {
      return err
    }
  }
  return tx.Commit()
}

func NewModels(scheme string, config *config.Config, a app.Application) (*sql.DB, models.SqlDialect, *services.Users, *services.Crypto, *services.OAuth2) {
  db, dialect, err := db.NewDB(config)
  if err != nil {
    util.ErrorLogger.Fatal(err)
  }

  clock, err := ap.NewClock(config.ActivityPubConfig.ClockTimezone)
  if err != nil {
    util.ErrorLogger.Fatal(err)
  }

  cryp, _, _, _, _, _, _, oauthSrv, _, _, _, users, _, _, ml := createModelsAndServices(
    config, db, dialect, a, config.ServerConfig.Host, scheme, clock,
  )
  for _, m := range ml {
    if err := m.Prepare(db, dialect); err != nil {
      util.ErrorLogger.Fatal(err)
    }
  }
  return db, dialect, users, cryp, oauthSrv
}

func createModelsAndServices(c *config.Config, sqldb *sql.DB, d models.SqlDialect, appl app.Application, host, scheme string, clock pub.Clock) (cryp *services.Crypto,
  data *services.Data,
  dAttempts *services.DeliveryAttempts,
  followers *services.Followers,
  following *services.Following,
  inboxes *services.Inboxes,
  liked *services.Liked,
  oauth *services.OAuth2,
  outboxes *services.Outboxes,
  policies *services.Policies,
  pkeys *services.PrivateKeys,
  users *services.Users,
  nodeinfo *services.NodeInfo,
  any *services.Any,
  m []models.Model) {
  us := &models.Users{}
  fd := &models.FedData{}
  ld := &models.LocalData{}
  in := &models.Inboxes{}
  ou := &models.Outboxes{}
  da := &models.DeliveryAttempts{}
  pk := &models.PrivateKeys{}
  ci := &models.ClientInfos{}
  ti := &models.TokenInfos{}
  cd := &models.Credentials{}
  fn := &models.Following{}
  fr := &models.Followers{}
  li := &models.Liked{}
  po := &models.Policies{}
  rs := &models.Resolutions{}
  m = []models.Model{
    us,
    fd,
    ld,
    in,
    ou,
    da,
    pk,
    ci,
    ti,
    cd,
    fn,
    fr,
    li,
    po,
    rs,
  }
  cryp = &services.Crypto{
    DB:    sqldb,
    Users: us,
  }
  dAttempts = &services.DeliveryAttempts{
    DB:               sqldb,
    DeliveryAttempts: da,
  }
  followers = &services.Followers{
    DB:        sqldb,
    Followers: fr,
  }
  following = &services.Following{
    DB:        sqldb,
    Following: fn,
  }
  inboxes = &services.Inboxes{
    DB:      sqldb,
    Inboxes: in,
  }
  liked = &services.Liked{
    DB:    sqldb,
    Liked: li,
  }
  data = &services.Data{
    DB:                    sqldb,
    Hostname:              host,
    FedData:               fd,
    LocalData:             ld,
    Users:                 us,
    Following:             following,
    Followers:             followers,
    Liked:                 liked,
    DefaultCollectionSize: c.DatabaseConfig.DefaultCollectionPageSize,
    MaxCollectionPageSize: c.DatabaseConfig.MaxCollectionPageSize,
  }
  oauth = &services.OAuth2{
    DB:     sqldb,
    Client: ci,
    Token:  ti,
    Creds:  cd,
  }
  outboxes = &services.Outboxes{
    DB:       sqldb,
    Outboxes: ou,
  }
  policies = &services.Policies{
    Clock:       clock,
    DB:          sqldb,
    Policies:    po,
    Resolutions: rs,
  }
  pkeys = &services.PrivateKeys{
    Scheme:      scheme,
    Host:        host,
    DB:          sqldb,
    PrivateKeys: pk,
  }
  users = &services.Users{
    App:         appl,
    DB:          sqldb,
    Users:       us,
    PrivateKeys: pk,
    Inboxes:     in,
    Outboxes:    ou,
    Followers:   fr,
    Following:   fn,
    Liked:       li,
  }
  nodeinfo = &services.NodeInfo{
    DB:               sqldb,
    Users:            us,
    LocalData:        ld,
    Rand:             rand.New(rand.NewSource(time.Now().UnixNano())),
    CacheInvalidated: time.Second * time.Duration(c.NodeInfoConfig.AnonymizedStatsCacheInvalidatedSeconds),
  }
  any = &services.Any{
    DB:      sqldb,
    Dialect: d,
  }
  return
}
