/*
 * Socialtap - Drink socially
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package database

import (
  "context"
  "net/url"

  "github.com/go-fed/activity/streams/vocab"
  "github.com/go-fed/apcore/app"
)

func GetPublicSearchResults(ctx context.Context, db app.Database, vocabType, search string) (result []vocab.Type, err error) {
  return getTypes(ctx, db, `WITH fed AS (
      SELECT
        DISTINCT ON(similarity) payload,
          similarity(concat(payload->'manufacturer'->>'name', payload->'drink'->>'name'), $2) AS similarity
      FROM %[1]sfed_data
      WHERE payload->>'type' = $1
      AND (
        payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
      )
      ORDER BY similarity DESC
      LIMIT 10
    ), loc AS (
      SELECT
        DISTINCT ON(similarity) payload,
          similarity(concat(payload->'manufacturer'->>'name', payload->'drink'->>'name'), $2) AS similarity
      FROM %[1]slocal_data
      WHERE payload->>'type' = $1
      AND (
        payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
      )
      ORDER BY similarity DESC
      LIMIT 10
    ), fin AS (
      SELECT payload, similarity FROM fed
      UNION
      SELECT payload, similarity from loc
    )
    SELECT payload
    FROM fin
    ORDER BY similarity DESC`, vocabType, search)
}

func GetPublicAndPrivateSearchResults(ctx context.Context, db app.Database, vocabType string, iri *url.URL, search string) (result []vocab.Type, err error) {
  return getTypes(ctx, db, `WITH fed AS (
      SELECT
        DISTINCT ON(similarity) payload,
          similarity(concat(payload->'manufacturer'->>'name', payload->'drink'->>'name'), $2) AS similarity
      FROM %[1]sfed_data
      WHERE payload->>'type' = $1
      AND (
        payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR payload->>'attributedTo' = $3
        OR payload->'to' ? $3
        OR payload->'cc' ? $3
      )
      ORDER BY similarity DESC
      LIMIT 10
    ), loc AS (
      SELECT
        DISTINCT ON(similarity) payload,
          similarity(concat(payload->'manufacturer'->>'name', payload->'drink'->>'name'), $2) AS similarity
      FROM %[1]slocal_data
      WHERE payload->>'type' = $1
      AND (
        payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR payload->>'attributedTo' = $3
        OR payload->'to' ? $3
        OR payload->'cc' ? $3
      )
      ORDER BY similarity DESC
      LIMIT 10
    ), fin AS (
      SELECT payload, similarity FROM fed
      UNION
      SELECT payload, similarity from loc
    )
    SELECT payload
    FROM fin
    ORDER BY similarity DESC`, vocabType, search, iri.String())
}
