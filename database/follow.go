/*
 * Socialtap - Drink socially
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package database

import (
  "context"
  "net/url"

  "github.com/go-fed/apcore/app"
  "github.com/go-fed/apcore/framework/config"
  "github.com/go-fed/apcore/util"
)

func FollowersContainsForActor(
  scheme string,
  config *config.Config,
  a app.Application,
  actor, item *url.URL) (contains bool, err error) {

  db, _, users, _, _ := NewModels(scheme, config, a)
  defer db.Close()

  ctx := util.Context{context.Background()}
  tx, err := db.BeginTx(ctx, nil)
  if err != nil {
    return contains, err
  }
  defer tx.Rollback()

  contains, err = users.Followers.ContainsForActor(ctx, tx, actor, item)
  if err != nil {
    return
  }
  return contains, tx.Commit()
}

func FollowersPrependItem(
  scheme string,
  config *config.Config,
  a app.Application,
  followers, item *url.URL) error {

  db, _, users, _, _ := NewModels(scheme, config, a)
  defer db.Close()

  ctx := util.Context{context.Background()}
  tx, err := db.BeginTx(ctx, nil)
  if err != nil {
    return err
  }
  defer tx.Rollback()

  err = users.Followers.PrependItem(ctx, tx, followers, item)
  if err != nil {
    return err
  }
  return tx.Commit()
}
