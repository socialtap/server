/*
 * Socialtap - Drink socially
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package database

import (
  "context"
  "net/url"

  "github.com/go-fed/activity/streams/vocab"
  "github.com/go-fed/apcore/app"
)

func GetPublicConversation(ctx context.Context, db app.Database, iri *url.URL) (notes []vocab.Type, err error) {
  return getTypes(ctx, db, `WITH RECURSIVE fed AS (
      SELECT payload, create_time FROM %[1]sfed_data WHERE payload->>'id' = $1
      AND (
        payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
      )
      UNION
      SELECT f.payload, f.create_time FROM %[1]sfed_data f JOIN fed r
        ON f.payload->>'inReplyTo' = r.payload->>'id'
      WHERE f.payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR f.payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
    ), fedb AS (
      SELECT payload, create_time FROM %[1]sfed_data WHERE payload->>'id' = $1
      AND (
        payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
      )
      UNION
      SELECT f.payload, f.create_time FROM %[1]sfed_data f JOIN fedb r
        ON f.payload->>'id' = r.payload->>'inReplyTo'
      WHERE f.payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR f.payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
    ), loc AS (
      SELECT payload, create_time FROM %[1]slocal_data WHERE payload->>'id' = $1
      AND (
        payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
      )
      UNION
      SELECT f.payload, f.create_time FROM %[1]slocal_data f JOIN loc r
        ON f.payload->>'inReplyTo' = r.payload->>'id'
      WHERE f.payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR f.payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
    ), locb AS (
      SELECT payload, create_time FROM %[1]slocal_data WHERE payload->>'id' = $1
      AND (
        payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
      )
      UNION
      SELECT f.payload, f.create_time FROM %[1]slocal_data f JOIN locb r
        ON f.payload->>'id' = r.payload->>'inReplyTo'
      WHERE f.payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR f.payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
    ), fin as (
      SELECT payload, create_time FROM fed
      UNION
      SELECT payload, create_time FROM loc
      UNION
      SELECT payload, create_time FROM fedb
      UNION
      SELECT payload, create_time FROM locb
    )
    SELECT payload FROM fin
    WHERE payload->'inReplyTo' IS NOT NULL
    ORDER BY create_time ASC`, iri.String())
}

func GetPublicOrPrivateConversation(ctx context.Context, db app.Database, iri, userIri *url.URL) (notes []vocab.Type, err error) {
  return getTypes(ctx, db, `WITH RECURSIVE fed AS (
      SELECT payload, create_time FROM %[1]sfed_data WHERE payload->>'id' = $1
      AND (
        payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR payload->>'attributedTo' = $2
        OR payload->'to' ? $2
        OR payload->'cc' ? $2
      )
      UNION
      SELECT f.payload, f.create_time FROM %[1]sfed_data f JOIN fed r
        ON f.payload->>'inReplyTo' = r.payload->>'id'
      WHERE f.payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR f.payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR f.payload->>'attributedTo' = $2
        OR f.payload->'to' ? $2
        OR f.payload->'cc' ? $2
    ), fedb AS (
      SELECT payload, create_time FROM %[1]sfed_data WHERE payload->>'id' = $1
      AND (
        payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR payload->>'attributedTo' = $2
        OR payload->'to' ? $2
        OR payload->'cc' ? $2
      )
      UNION
      SELECT f.payload, f.create_time FROM %[1]sfed_data f JOIN fedb r
        ON f.payload->>'id' = r.payload->>'inReplyTo'
      WHERE f.payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR f.payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR f.payload->>'attributedTo' = $2
        OR f.payload->'to' ? $2
        OR f.payload->'cc' ? $2
    ), loc AS (
      SELECT payload, create_time FROM %[1]slocal_data WHERE payload->>'id' = $1
      AND (
        payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR payload->>'attributedTo' = $2
        OR payload->'to' ? $2
        OR payload->'cc' ? $2
      )
      UNION
      SELECT f.payload, f.create_time FROM %[1]slocal_data f JOIN loc r
        ON f.payload->>'inReplyTo' = r.payload->>'id'
      WHERE f.payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR f.payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR f.payload->>'attributedTo' = $2
        OR f.payload->'to' ? $2
        OR f.payload->'cc' ? $2
    ), locb AS (
      SELECT payload, create_time FROM %[1]slocal_data WHERE payload->>'id' = $1
      AND (
        payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR payload->>'attributedTo' = $2
        OR payload->'to' ? $2
        OR payload->'cc' ? $2
      )
      UNION
      SELECT f.payload, f.create_time FROM %[1]slocal_data f JOIN locb r
        ON f.payload->>'id' = r.payload->>'inReplyTo'
      WHERE f.payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR f.payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
        OR f.payload->>'attributedTo' = $2
        OR f.payload->'to' ? $2
        OR f.payload->'cc' ? $2
    ), fin as (
      SELECT payload, create_time FROM fed
      UNION
      SELECT payload, create_time FROM loc
      UNION
      SELECT payload, create_time FROM fedb
      UNION
      SELECT payload, create_time FROM locb
    )
    SELECT payload FROM fin
    WHERE payload->'inReplyTo' IS NOT NULL
    ORDER BY create_time ASC`,
    iri.String(), userIri.String())
}
