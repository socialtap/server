// apcore is a server framework for implementing an ActivityPub application.
// Copyright (C) 2019 Cory Slep
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package database

import (
  "fmt"
  "context"
  "net/url"

  "github.com/go-fed/activity/streams"
  "github.com/go-fed/activity/streams/vocab"

  "github.com/go-fed/apcore/app"
  "github.com/go-fed/apcore/models"
  "github.com/go-fed/apcore/util"
  "github.com/go-fed/apcore/services"
  "github.com/go-fed/apcore/paths"
  "github.com/go-fed/apcore/framework/config"
)

func GetLatestPublicTypes(ctx context.Context, db app.Database, page int, vocabType string) (notes []vocab.Type, err error) {
  var (
    limit int = 10
    offset int = 0
  )
  if page > 1 {
    offset = (page - 1) * 10
  }

  return getTypes(ctx, db, `WITH local_notes AS(
  SELECT payload, create_time FROM %[1]slocal_data
  WHERE payload->>'type' = $1 AND (
    payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
    OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public')
), fed_notes AS (
  SELECT payload, create_time FROM %[1]sfed_data
  WHERE payload->>'type' = $1 AND (
    payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
    OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public')
), unioned AS (
SELECT payload, create_time FROM local_notes
UNION
SELECT payload, create_time FROM fed_notes
), deduped AS (
  SELECT
    DISTINCT ON (payload->'id') payload,
    create_time
  FROM unioned
  ORDER BY payload->'id'
)
SELECT payload FROM deduped
ORDER BY payload->'published' DESC
LIMIT $2 OFFSET $3`, vocabType, limit, offset)
}

func GetLatestPublicAndPrivateTypes(ctx context.Context, db app.Database, page int, userIRI, vocabType string) (notes []vocab.Type, err error) {
  var (
    limit int = 10
    offset int = 0
  )
  if page > 1 {
    offset = (page - 1) * 10
  }
  return getTypes(ctx, db, `WITH local_notes AS(
  SELECT payload, create_time FROM %[1]slocal_data
  WHERE payload->>'type' = $2 AND (
    payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
    OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
    OR payload->>'attributedTo' = $1
    OR payload->'to' ? $1
    OR payload->'cc' ? $1)
), fed_notes AS (
  SELECT payload, create_time FROM %[1]sfed_data
  WHERE payload->>'type' = $2 AND (
    payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
    OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
    OR payload->>'attributedTo' = $1
    OR payload->'to' ? $1
    OR payload->'cc' ? $1)
), unioned AS (
SELECT payload, create_time FROM local_notes
UNION
SELECT payload, create_time FROM fed_notes
), deduped AS (
  SELECT
    DISTINCT ON (payload->'id') payload,
    create_time
  FROM unioned
  ORDER BY payload->'id'
)
SELECT payload
FROM deduped
ORDER BY payload->'published' DESC
LIMIT $3 OFFSET $4`, userIRI, vocabType, limit, offset)
}

func GetPersonPublicTypes(ctx context.Context, db app.Database, page int, userIRI, vocabType string) (notes []vocab.Type, err error) {
  var (
    limit int = 10
    offset int = 0
  )
  if page > 1 {
    offset = (page - 1) * 10
  }
  return getTypes(ctx, db, `WITH local_notes AS(
  SELECT payload, create_time FROM %[1]slocal_data
  WHERE payload->>'type' = $2 AND (
    payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
    OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
  ) AND payload->>'attributedTo' = $1
), fed_notes AS (
  SELECT payload, create_time FROM %[1]sfed_data
  WHERE payload->>'type' = $2 AND (
    payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
    OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
  ) AND payload->>'attributedTo' = $1
), unioned AS (
SELECT payload, create_time FROM local_notes
UNION
SELECT payload, create_time FROM fed_notes
), deduped AS (
  SELECT
    DISTINCT ON (payload->'id') payload,
    create_time
  FROM unioned
  ORDER BY payload->'id'
)
SELECT payload
FROM deduped
ORDER BY payload->'published' DESC
LIMIT $3 OFFSET $4`, userIRI, vocabType, limit, offset)
}

func getTypes(ctx context.Context, db app.Database, sql string, data ...interface{}) (notes []vocab.Type, err error) {
  // XXX for debug purposes
  //util.InfoLogger.Infof("sql to be executed\n%s", sql)

  c := util.Context{ctx}
  rz, err := streams.NewTypeResolver(
    func(c context.Context, note vocab.ActivityStreamsNote) error {
      notes = append(notes, note)
      return nil
    },
    func(c context.Context, note vocab.SocialtapCheckin) error {
      notes = append(notes, note)
      return nil
    },
  )
  if err != nil {
    return nil, fmt.Errorf("cannot create type resolver: %s", err)
  }
  txb := db.Begin()
  txb.Query(sql,
    func(r app.SingleRow) error {
      var v models.ActivityStreams
      if err := r.Scan(&v); err != nil {
        return err
      }
      return rz.Resolve(c.Context, v.Type)
    },
    data...)
  err = txb.Do(c)
  return
}

func GetUsers(ctx context.Context, db app.Database) (ppl []vocab.Type, err error) {
  c := util.Context{ctx}
  var rz *streams.TypeResolver
  rz, err = streams.NewTypeResolver(func(c context.Context, pn vocab.ActivityStreamsPerson) error {
    ppl = append(ppl, pn)
    return nil
  })
  if err != nil {
    return
  }
  txb := db.Begin()
  txb.Query(`SELECT actor FROM %[1]susers
WHERE actor->>'type' = 'Person'
ORDER BY create_time DESC`,
    func(r app.SingleRow) error {
      var v models.ActivityStreams
      if err := r.Scan(&v); err != nil {
        return err
      }
      return rz.Resolve(c.Context, v.Type)
    })
  err = txb.Do(c)
  return
}

func GetTypeIsReadable(ctx util.Context, db app.Database, noteID, userID *url.URL, vocabType string) (legible /*lol*/ bool, err error) {
  txb := db.Begin()
  txb.Query(`SELECT EXISTS (
SELECT create_time FROM %[1]slocal_data
WHERE payload->>'type' = $3 AND (
  payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
  OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public'
  OR payload->>'attributedTo' = $2
  OR payload->'to' ? $2
  OR payload->'cc' ? $2)
  AND payload->>'id' = $1)`,
    func(r app.SingleRow) error {
      return r.Scan(&legible)
    },
    noteID.String(), userID.String(), vocabType)
  err = txb.Do(ctx)
  return
}

func GetTypeIsPublic(ctx util.Context, db app.Database, noteID *url.URL, vocabType string) (pub bool, err error) {
  txb := db.Begin()
  txb.Query(`SELECT EXISTS (
SELECT create_time FROM %[1]slocal_data
WHERE payload->>'type' = $2 AND (
  payload->'to' ? 'https://www.w3.org/ns/activitystreams#Public'
  OR payload->'cc' ? 'https://www.w3.org/ns/activitystreams#Public')
  AND payload->>'id' = $1)`,
    func(r app.SingleRow) error {
      return r.Scan(&pub)
    },
    noteID.String(), vocabType)
  err = txb.Do(ctx)
  return
}

func UpdateUserPasswordAndSalt(ctx util.Context, db app.Database, id paths.UUID, hash, salt []byte) error {
  txb := db.Begin()
  txb.ExecOneRow(
    `UPDATE %[1]susers SET salt = $1, hashpass = $2 WHERE id = $3`, salt, hash, string(id))
  return txb.Do(ctx)
}

func FindUserByEmail(ctx util.Context, db app.Database, email string) (id string, err error) {
  txb := db.Begin()
  txb.Query(`SELECT id FROM %[1]susers WHERE email = $1 LIMIT 1`,
    func(r app.SingleRow) error {
      return r.Scan(&id)
    },
    email)
  err = txb.Do(ctx)
  return
}

func CreateUser(
  scheme string,
  config *config.Config,
  a app.Application,
  username, email, password string) (string, error) {

  db, _, users, _, _ := NewModels(scheme, config, a)
  defer db.Close()

  tx, err := db.BeginTx(context.Background(), nil)
  if err != nil {
    return "", err
  }
  defer tx.Rollback()

  user := services.CreateUserParameters{
    Scheme: scheme,
    Host: config.ServerConfig.Host,
    Username: username,
    Email: email,
    HashParams: services.HashPasswordParameters{
      SaltSize: config.ServerConfig.SaltSize,
      BCryptStrength: config.ServerConfig.BCryptStrength,
    },
    RSAKeySize: config.ServerConfig.RSAKeySize,
  }

  id, err := users.CreateUser(util.Context{context.Background()}, user, password)
  if err != nil {
    return id, err
  }
  return id, tx.Commit()
}

func FindApcoreUserByUsername(
  scheme string,
  config *config.Config,
  a app.Application,
  username string) (*services.User, error) {

  db, _, users, _, _ := NewModels(scheme, config, a)
  defer db.Close()

  return users.UserByUsername(util.Context{context.Background()}, username)
}

func FindUserPreferences(
  scheme string,
  config *config.Config,
  a app.Application,
  id paths.UUID) (*services.Preferences, error) {

  db, _, users, _, _ := NewModels(scheme, config, a)
  defer db.Close()

  ctx := util.Context{context.Background()}
  prefs := make(map[string]interface{})
  return users.Preferences(ctx, id, &prefs)
}

func UpdateUserPreferences(
  scheme string,
  config *config.Config,
  a app.Application,
  id string,
  p *services.Preferences) error {

  db, _, users, _, _ := NewModels(scheme, config, a)
  defer db.Close()

  ctx := util.Context{context.Background()}
  tx, err := db.BeginTx(ctx, nil)
  if err != nil {
    util.ErrorLogger.Errorln(err)
    return err
  }
  defer tx.Rollback()

  err = users.UpdatePreferences(ctx, id, p)
  if err != nil {
    util.ErrorLogger.Errorln(err)
    return err
  }
  return tx.Commit()
}
