package database
/*
 * Socialtap - Drink socially
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
  "fmt"
  "net/url"
  "context"

  "github.com/go-fed/activity/streams"
  "github.com/go-fed/apcore/models"
  "github.com/go-fed/activity/streams/vocab"
  "github.com/go-fed/apcore/app"
  "github.com/go-fed/apcore/util"
)

func UniqueCheckinCounterByIRI(ctx context.Context, db app.Database, iri *url.URL) (cnt int, err error) {
  c := util.Context{ctx}
  txb := db.Begin()
  txb.Query(`WITH dist AS (
    SELECT
      DISTINCT
        UPPER(payload->'drink'->>'category'),
        UPPER(payload->'drink'->>'name')
    FROM %[1]slocal_data
    WHERE payload->>'type' = 'Checkin'
    AND payload->>'attributedTo' = $1
  ) SELECT COUNT(*) from dist`,
    func(r app.SingleRow) error {
      return r.Scan(&cnt)
    },
    iri.String())
  err = txb.Do(c)
  return
}

func TotalCheckinsByIRI(ctx context.Context, db app.Database, iri *url.URL) ([]vocab.SocialtapCheckin, error) {
  return getCheckins(ctx, db, `SELECT payload
    FROM %[1]slocal_data
    WHERE payload->>'type' = 'Checkin'
    AND payload->>'attributedTo' = $1`, iri.String())
}


func getCheckins(ctx context.Context, db app.Database, sql string, data... interface{}) (checkins []vocab.SocialtapCheckin, err error) {
  c := util.Context{ctx}
  rz, err := streams.NewTypeResolver(
    func(c context.Context, checkin vocab.SocialtapCheckin) error {
      checkins = append(checkins, checkin)
      return nil
    },
  )
  if err != nil {
    return nil, fmt.Errorf("cannot create type resolver: %s", err)
  }
  txb := db.Begin()
  txb.Query(sql,
    func(r app.SingleRow) error {
      var v models.ActivityStreams
      if err := r.Scan(&v); err != nil {
        return err
      }
      return rz.Resolve(c.Context, v.Type)
    },
    data...)
  err = txb.Do(c)
  return
}
