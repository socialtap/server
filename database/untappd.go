/*
 * Socialtap - Drink socially
 * Copyright (C) 2021 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package database

import (
  "fmt"
  "net/url"
  "context"

  "github.com/go-fed/activity/streams/vocab"
  "github.com/go-fed/apcore/app"
)

func GetOldestUntappdCheckin(ctx context.Context, db app.Database, userIRI *url.URL) (vocab.SocialtapCheckin, error) {

  checkins, err := getCheckins(ctx, db, `SELECT payload FROM %[1]slocal_data
    WHERE payload->>'type' = 'Checkin'
    AND payload->'untappdCheckinId' IS NOT NULL
    AND payload->>'attributedTo' = $1
    ORDER BY payload->'untappdCheckinId' ASC
    LIMIT 1`, userIRI.String())

  if len(checkins) == 0 {
    return nil, fmt.Errorf("no checkin found")
  }
  return checkins[0], err
}
