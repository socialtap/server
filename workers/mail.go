package workers
/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2019 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import (
  "html/template"
  "bytes"

  "github.com/go-gomail/gomail"
  "github.com/go-fed/apcore/util"
  "github.com/go-fed/apcore/framework/config"
  "github.com/microcosm-cc/bluemonday"
)

const mailTmpl = `<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
    <div style="overflow: auto;">
      <div style="float: left;">
        <svg style="width: 100px" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="beer" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-beer fa-w-14 fa-9x"><path fill="currentColor" d="M368 96h-48V56c0-13.255-10.745-24-24-24H24C10.745 32 0 42.745 0 56v400c0 13.255 10.745 24 24 24h272c13.255 0 24-10.745 24-24v-42.11l80.606-35.977C429.396 365.063 448 336.388 448 304.86V176c0-44.112-35.888-80-80-80zm16 208.86a16.018 16.018 0 0 1-9.479 14.611L320 343.805V160h48c8.822 0 16 7.178 16 16v128.86zM208 384c-8.836 0-16-7.164-16-16V144c0-8.836 7.164-16 16-16s16 7.164 16 16v224c0 8.836-7.164 16-16 16zm-96 0c-8.836 0-16-7.164-16-16V144c0-8.836 7.164-16 16-16s16 7.164 16 16v224c0 8.836-7.164 16-16 16z" class=""></path></svg>
      </div>
      <div style="padding-left: 120px">
        {{safehtml .Text}}
      </div>
    </div>
    <hr color="#999">
    <p style="font-size: 12px; color: #999;">
      This mail was sent by Socialtap.
    </p>
  </body>
</html>`

var funcMap = template.FuncMap{
  "safehtml": func(text string) template.HTML {
    p := bluemonday.UGCPolicy()
    return template.HTML(p.Sanitize(text))
  },
  "striphtml": func(text string) template.HTML {
    p := bluemonday.StrictPolicy()
    return template.HTML(p.Sanitize(text))
  },
}

func MailWorker(config *config.Config, to, subject, text string) {
  if !config.SocialtapConfig.Mail {
    util.InfoLogger.Infof("Mail is disabled. Skipping it!")
    return
  }

  msg := gomail.NewMessage()
  tmpl, err := template.New("mail.html").Funcs(funcMap).Parse(mailTmpl)
  if err != nil {
    util.ErrorLogger.Errorf("cannot parse template: %s", err)
    return
  }

  var buffer bytes.Buffer
  err = tmpl.ExecuteTemplate(&buffer, "mail.html", map[string]interface{}{"Text": text})
  if err != nil {
    util.ErrorLogger.Errorf("cannot execute template: %s", err)
    return
  }

  msg.SetHeader("From", config.SocialtapConfig.SMTPFrom)
  msg.SetHeader("To", to)
  msg.SetHeader("Subject", subject)
  msg.SetBody("text/html", buffer.String())

  dialer := gomail.NewDialer(config.SocialtapConfig.SMTPHost,
    config.SocialtapConfig.SMTPPort,
    config.SocialtapConfig.SMTPUsername,
    config.SocialtapConfig.SMTPPassword)
  //if config.SocialtapConfig.SMTPInsecure {
  //  dialer.TLSConfig = &tls.Config{InsecureSkipVerify: true}
  //}
  err = dialer.DialAndSend(msg)
  if err != nil {
    util.ErrorLogger.Errorf("cannot send mail: %s", err)
    return
  }
}
