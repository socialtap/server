/*
 * Socialtap - Visualize your Untappd checkins
 * Copyright (C) 2019 Lukas Matt <lukas@matt.wf>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package workers

import (
  "time"
  "net/url"
  "net/http"
  "strings"
  "strconv"
  "encoding/json"
  "context"
  "fmt"
  "math"

  "github.com/go-fed/apcore/services"
  "github.com/go-fed/apcore/paths"
  "github.com/go-fed/apcore/util"
  "github.com/go-fed/apcore/app"
  "github.com/go-fed/apcore/framework/config"
  "github.com/go-fed/activity/streams/vocab"
  "github.com/mdlayher/untappd"

  "git.feneas.org/socialtap/server/database"
  builder "git.feneas.org/socialtap/server/vocab_builder"
)

// UntappdHandler will fetch all people from database and run the UntappdCheckinWorker
func UntappdHandler(
  config *config.Config,
  scheme string,
  framework app.Framework,
  a app.Application,
  db app.Database) func() {

  return func() {
    ctx := util.Context{context.Background()}
    ppl, err := database.GetUsers(ctx, db)
    if err != nil {
      util.ErrorLogger.Errorln(err)
      return
    }

    pplLoop:
    for _, p := range ppl {
      if person, ok := p.(vocab.ActivityStreamsPerson); ok {
        personIdProp := person.GetJSONLDId()
        idParts := strings.Split(personIdProp.Get().String(), "/")
        if len(idParts) < 3 {
          util.ErrorLogger.Errorf("cannot extract UUID from user IRI: %s", personIdProp.Get())
          continue pplLoop
        }
        // NOTE this assumes that the uuid is always the last item
        uuid := paths.UUID(idParts[len(idParts) - 1])
        // debug logging
        if config.SocialtapConfig.Debug {
          util.InfoLogger.Infof("running untappd checkin worker for user %s", uuid)
        }
        // run worker
        err = UntappdCheckinWorker(config, scheme, framework, a, db, uuid)
        if err != nil {
          util.ErrorLogger.Errorf("untappd checkin worker: %s", err)
        }
      }
    }
  }
}

// UntappdCheckinWorker will fetch all checkins from api.untappd.com.
func UntappdCheckinWorker(
  config *config.Config,
  scheme string,
  framework app.Framework,
  a app.Application,
  db app.Database,
  userID paths.UUID) error {

  ctx := util.Context{context.Background()}
  userIRI := paths.UUIDIRIFor(scheme, config.ServerConfig.Host, paths.UserPathKey, userID)

  prefs, prefMap, err := userPreferences(config, scheme, a, userID)
  if err != nil {
    return err
  }

  token, ok := prefMap["UntappdToken"].(string); if !ok {
    return fmt.Errorf("user (%s) is not authenticated with untappd: %+v", userID, prefs)
  }

  client, err := untappd.NewAuthenticatedClient(token, nil)
  if err != nil {
    return err
  }

  var oldestID float64 = 0
  if _, ok := prefMap["UntappdInitSync"]; !ok {
    checkin, _ := database.GetOldestUntappdCheckin(ctx, db, userIRI)
    if checkin != nil {
      idProp := checkin.GetSocialtapUntappdCheckinId()
      if !idProp.Empty() {
        oldestID = idProp.At(0).Get()
      }
    }

    processingLoop:
    for {
      util.InfoLogger.Infof("%s processing checkin id %.0f\n", userIRI, oldestID)

      cnt, err := fetchAndPostCheckins(ctx, db, userID, userIRI, &oldestID, client, framework)
      if err != nil {
        return err
      }

      if err == nil && cnt == 0 {
        // we synced successfully! let's avoid re-runs in future
        prefMap["UntappdInitSync"] = 1
        prefs.AppPreferences = prefMap
        err = database.UpdateUserPreferences(scheme, config, a, string(userID), prefs)
        if err != nil {
          return err
        }
        break processingLoop
      }
    }
  } else {
    _, err := fetchAndPostCheckins(ctx, db, userID, userIRI, &oldestID, client, framework)
    if err != nil {
      return err
    }
  }
  return nil
}

// UntappdSendCheckinWorker builds a Checkin type from a json map and
// sends the result to Untappd
func UntappdSendCheckinWorker(
  config *config.Config,
  scheme string,
  framework app.Framework,
  a app.Application,
  userID paths.UUID,
  beerID int,
  params map[string]interface{}) error {

  prefs, prefMap, err := userPreferences(config, scheme, a, userID)
  if err != nil {
    return err
  }

  token, ok := prefMap["UntappdToken"].(string); if !ok {
    return fmt.Errorf("user (%s) is not authenticated with untappd: %+v", userID, prefs)
  }

  client, err := untappd.NewAuthenticatedClient(token, nil)
  if err != nil {
    return err
  }

  var userRating, lat, lng float64
  if location, ok := params["venueLocation"].([]interface{}); ok {
    if len(location) > 1 {
      var err error
      latString, _ := location[0].(string)
      lngString, _ := location[1].(string)
      lat, err = strconv.ParseFloat(latString, 64)
      if err != nil {
        util.ErrorLogger.Infof("cannot parse latitude: %s", latString)
      }
      lng, err = strconv.ParseFloat(lngString, 64)
      if err != nil {
        util.ErrorLogger.Infof("cannot parse longitude: %s", lngString)
      }
    }
  }

  comment, _ := params["checkinComment"].(string)

  if rating, ok := params["checkinRating"].([]interface{}); ok {
    if len(rating) > 1 {
      userRating, _ = rating[0].(float64)
    }
  }
  // sanity checks for the rating
  if userRating < 1 || userRating > 5 {
    userRating = 0.0 // must be between 1 and 5
  } else {
    // untappd only allows .25, .5 and .75 as decimal values
    // this is the lazy version of enforcing this rule :\
    userRating = math.Trunc(userRating)
  }

  timezone, offset := time.Now().Zone()
  request := untappd.CheckinRequest{
    BeerID: beerID,
    GMTOffset: offset / 60 / 60,
    TimeZone: timezone,
    Latitude: lat,
    Longitude: lng,
    Comment: comment,
    Rating: userRating,
  }

  _, resp, err := client.Auth.Checkin(request)
  if err != nil {
    return err
  }

  if resp.StatusCode != http.StatusOK {
    util.ErrorLogger.Infof("untappd did not return with status OK: %+v", resp)
  }
  return nil
}

func fetchAndPostCheckins(
  ctx util.Context,
  db app.Database,
  userID paths.UUID,
  userIRI *url.URL,
  id *float64,
  client *untappd.Client,
  framework app.Framework) (int, error) {

  if id == nil {
    return -1, fmt.Errorf("id must not be nil")
  }

  checkins, resp, err := client.User.CheckinsMinMaxIDLimit("",
    0,        // min id will be ignored if set to zero
    int(*id), // set the max id as limit
    50,       // request the max possible amount of checkins
  ); if err != nil {
    return -1, err
  }
  defer resp.Body.Close()

  var totalCheckins = len(checkins)
  if totalCheckins == 0 {
    // avoid a re-run if the last result was empty
    util.InfoLogger.Infof("%s has no more checkins", userID)
    util.InfoLogger.Infof("response status: %s\n%+v", resp.Status, resp.Header)

    var remainingLimit int
    if rateLimit, ok := resp.Header["X-Ratelimit-Remaining"]; ok {
      remainingLimit, err = strconv.Atoi(rateLimit[0])
      if err != nil {
        return -1, err
      }
    }

    if remainingLimit <= 0 {
      return -1, fmt.Errorf("%s hit the rate limit", userID)
    }
    return 0, nil
  }

  dbCheckins, err := database.TotalCheckinsByIRI(ctx, db, userIRI)
  if err != nil {
    return -1, err
  }

  // update max ID
  for _, checkin := range checkins {
    checkinID := float64(checkin.ID)
    if *id == 0 || checkinID < *id {
      *id = checkinID
    }
  }

  // check if the certain entries already exist in the database
  dbCheckinLoop:
  for _, dbCheckin := range dbCheckins {
    idProp := dbCheckin.GetSocialtapUntappdCheckinId()
    if idProp.Empty() {
      continue dbCheckinLoop
    }

    dbID := int(idProp.At(0).Get())
    for idx, checkin := range checkins {
      if dbID == checkin.ID {
        if len(checkins) == 1 {
          return 0, nil
        }
        // remove entry from untappd checkins
        checkins[idx] = checkins[len(checkins)-1]
        checkins = checkins[:len(checkins)-1]
      }
    }
  }

  // add the remaining entries to the database
  for _, checkin := range checkins {
    apCheckin, err := builder.ConvertUntappdCheckin(checkin)
    if err != nil {
      return -1, fmt.Errorf("cannot convert untappd checkin: %s", err)
    }

    if err := framework.Send(ctx, userID, apCheckin); err != nil {
      return -1, fmt.Errorf("cannot send activity (%s): %s", userID, err)
    }
  }

  util.InfoLogger.Infof("%d checkins processed\n", totalCheckins)
  return totalCheckins, nil
}

// UntappdSearchBeerWorker will fetch search results from untappd.com
func UntappdSearchBeerWorker(
  config *config.Config,
  scheme string,
  a app.Application,
  db app.Database,
  userID paths.UUID,
  searchText string) (beers []*untappd.Beer, err error) {

  prefs, prefMap, err := userPreferences(config, scheme, a, userID)
  if err != nil {
    return beers, err
  }

  token, ok := prefMap["UntappdToken"].(string); if !ok {
    return beers, fmt.Errorf("user (%s) is not authenticated with untappd: %+v", userID, prefs)
  }

  client, err := untappd.NewAuthenticatedClient(token, nil)
  if err != nil {
    return beers, err
  }

  beers, _, err = client.Beer.SearchOffsetLimitSort(searchText, 0, 10, untappd.SortCheckin)
  return
}

func userPreferences(
  config *config.Config,
  scheme string,
  a app.Application,
  userID paths.UUID) (*services.Preferences, map[string]interface{}, error) {

  prefs, err := database.FindUserPreferences(scheme, config, a, userID)
  if err != nil {
    return nil, nil, err
  }

  var prefMap map[string]interface{}
  prefBytes, err := json.Marshal(prefs.AppPreferences)
  if err != nil {
    return nil, nil, err
  }
  err = json.Unmarshal(prefBytes, &prefMap)
  if err != nil {
    return nil, nil, err
  }
  // in case prefBytes equals null
  if prefMap == nil {
    prefMap = make(map[string]interface{})
  }
  return prefs, prefMap, nil
}
